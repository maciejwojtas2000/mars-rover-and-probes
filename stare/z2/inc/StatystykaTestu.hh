#ifndef STATYSTYKATESTU_HH
#define STATYSTYKATESTU_HH
#include<iostream>

struct StatystykaTestu {
    int dobre;
    int zle;
};

void Wyswietl( StatystykaTestu  StatTestu );
StatystykaTestu Tworz(int dobre, int zle);
void Aktualizuj(StatystykaTestu StatTestu, bool odp);

#endif

