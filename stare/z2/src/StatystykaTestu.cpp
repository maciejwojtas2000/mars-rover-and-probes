#include "StatystykaTestu.hh"

void Wyswietl(StatystykaTestu StatTestu)
{
    std::cout<<"Odpowiedziano na"<<" "<<StatTestu.dobre+StatTestu.zle<<" "<<"odpowiedzi"<<std::endl;
    std::cout<<"Uzyskano"<<" "<<StatTestu.dobre<<" "<<"dobrych odpowiedzi."<<std::endl;
    std::cout<<"Uzyskano"<<" "<<StatTestu.zle<<"złych odpowiedzi."<<" "<<std::endl;
    std::cout<<"Uzyskano"<<" "<<StatTestu.dobre*100/(StatTestu.dobre+StatTestu.zle)<<"%"<<" "<<"dobrych odpowiedzi"<<std::endl;
}

StatystykaTestu Tworz(int dobre, int zle)
{
    StatystykaTestu Stat;
    Stat.dobre=dobre;
    Stat.zle=zle;
    return Stat;
}

void Aktualizuj(StatystykaTestu &StatTestu, bool odp)
{
    if(odp==0) 
    {
        StatTestu.zle=StatTestu.zle+1;
        std::cout<<"Zaktualizowano złe"<<std::endl;
    }
    else if(odp==1)
    {
    StatTestu.dobre=StatTestu.dobre+1;
    std::cout<<StatTestu.dobre;
    std::cout<<"Zaktualizowano dobre"<<std::endl;
    }
}