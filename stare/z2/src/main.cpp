#include <iostream>
#include <fstream>
#include "WyrazenieZesp.hh"
#include "StatystykaTestu.hh"



void WykonajTest(std::istream &rStrmWej)
{
LZespolona Odp;
StatystykaTestu Stat={0,0};
WyrazenieZesp Pytanie;

  std::cout<<"Wykonaj test:"<<std::endl;
  while (WczytajWyrazenieZesp(rStrmWej,Pytanie)) 
  {
    std::cout<<"Oblicz:";
    std::cout<<Pytanie;
    std::cout<<std::endl;

    for(int i=0;i<3;i++)
    {
      std::cin>>Odp;
      if(std::cin.fail())
      {
          std::cout<<"Błąd zapisu. Spróbuj jeszcze raz"<<std::endl;
          std::cin.clear();
          std::cin.ignore(1000,'\n');
      }
      else
      {
        break;
      }
    }
    if(Odp==Oblicz(Pytanie))
    {
      std::cout<<"Odpowiedź poprawna"<<std::endl;
      
      Stat.dobre++;
    }
    else
    {
      std::cout<<"Błąd. Poprawny wynik to: "<<Oblicz(Pytanie);
      Stat.zle++;
    }
  }
  Wyswietl(Stat);
}






int main(int argc, char **argv)
{

  if (argc < 2) {
    std::cerr << std::endl;
    std::cerr << " Brak nazwy pliku z zawartoscia testu." << std::endl;
    std::cerr << std::endl;
    return 1;
  }

  std::ifstream  PlikTestu(argv[1]);

  if (PlikTestu.is_open() == false) {
    //
    // Tu trzeba wpisac wyswietlenie informacji, ze nie
    // mozna otworzyc pliku i nalezy wyswietlic jego nazwe.
    //
    return 1;
  }
WykonajTest(PlikTestu);

  std::cout << std::endl;
  std::cout << " Start testu arytmetyki zespolonej: " << argv[1] << std::endl;
  std::cout << std::endl;
}
  

 
    
  
