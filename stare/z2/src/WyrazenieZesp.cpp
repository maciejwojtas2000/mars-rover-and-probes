#include <iostream>
#include "WyrazenieZesp.hh"
#include "LZespolona.hh"


/*
 * Tu nalezy zdefiniowac funkcje, ktorych zapowiedzi znajduja sie
 * w pliku naglowkowym.
 */
std::istream &operator>>(std::istream &strm, Operator & Op)
{
  char znak;
  strm>>znak;
  switch(znak){
    case '+':{ 
      Op=Op_Dodaj;   break;
    }
    case '-':{
       Op=Op_Odejmij; break;
    }
    case '*':{

     Op=Op_Mnoz;    break;
    }
    case '/':{
       Op=Op_Dziel;   break;
    }
    default:  strm.setstate(std::ios::failbit);
  }
  return strm;
}

std::ostream & operator<<(std::ostream &strm, const Operator & Op)
{
  switch(Op){
    case Op_Dodaj: strm<<'+';   break;
    case Op_Odejmij: strm<<'-'; break;
    case Op_Mnoz: strm<<'*';    break;
    case Op_Dziel: strm<<'/';   break;

    default:  strm.setstate(std::ios::failbit);
  }
  return strm;
}





std::ostream & operator <<(std::ostream &strm, const WyrazenieZesp & WyrZ)
{
  strm<<WyrZ.Arg1<<WyrZ.Op<<WyrZ.Arg2;
  return strm;
}

std::istream & operator >> (std::istream &strm, WyrazenieZesp &WyrZ)
{
  //char znak;
  strm>>WyrZ.Arg1>>WyrZ.Op>>WyrZ.Arg2;//>>znak;;
//  std::cout << WyrZ.Op << std::endl << std::flush;
  return strm;
}
bool WczytajWyrazenieZesp(std::istream &rStrmWe,WyrazenieZesp &rWyrZ)
{
 // char end;
  rStrmWe>>rWyrZ;
  
  if(rStrmWe.fail()==true||rStrmWe.eof())
  {
    return false;
  }
  else
  {
      return true;
  }
}

LZespolona Oblicz(WyrazenieZesp WyrZ)
{
  LZespolona Wynik;

  if(WyrZ.Op==Op_Dodaj)
  Wynik=WyrZ.Arg1+WyrZ.Arg2;

  else if(WyrZ.Op==Op_Odejmij)
  Wynik=WyrZ.Arg1-WyrZ.Arg2;

  else if(WyrZ.Op==Op_Mnoz)
  Wynik=WyrZ.Arg1*WyrZ.Arg2;

  else if(WyrZ.Op==Op_Dziel)
  Wynik=WyrZ.Arg1/WyrZ.Arg2;

  else std::cerr<<"Błędny operator"<<std::endl;
  
  return Wynik;
}

WyrazenieZesp Tworz(LZespolona Arg1, Operator Op, LZespolona Arg2){
  WyrazenieZesp Wynik;
  Wynik.Arg1=Arg1;
  Wynik.Op=Op;
  Wynik.Arg2=Arg2;

  return Wynik;  
}

