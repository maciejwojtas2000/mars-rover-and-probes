#include "LZespolona.hh"



/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
bool Wczytaj(LZespolona Wynik)
{
  
  char znak;
  std::cin>>znak;
  if(znak != '(')
  {
    return false;
  };
  std::cin>>Wynik.re;
  std::cin>>Wynik.im;
  std::cin>>znak;
  if(znak!='i')
  {
    return false;
  };
  std::cin>>znak;
  if(znak!=')')
  {
    return false;
  }
  return true;
}
double modul2(LZespolona Skl1)
{  
  double Wynik;

  Wynik=Skl1.re*Skl1.re+Skl1.im*Skl1.im;
  return Wynik;
}
std::ostream & operator <<(std::ostream &strm, const LZespolona & Z1)
{
  strm<<"("<<Z1.re<<std::showpos<<Z1.im<<std::noshowpos<<"i)";
  return strm;
}
std::istream & operator >>(std::istream &strm, LZespolona & Z1)
{
  char znak;
  strm>>znak;
  if(znak !='(')
  {
    strm.setstate(std::ios::failbit);
  }
  strm>>Z1.re;
  strm>>Z1.im;
  strm>>znak;
  if(znak !='i')
  {
    strm.setstate(std::ios::failbit);
  }
  strm>>znak;
  if(znak !=')')
  {
    strm.setstate(std::ios::failbit);
  }


  strm.setstate(std::ios::goodbit);
  return strm;
}

LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  return Wynik;
}
LZespolona  operator - (LZespolona  Skl1,  LZespolona  Skl2)
{
   LZespolona  Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;
  return Wynik;
}
LZespolona  operator * (LZespolona  Skl1,  LZespolona  Skl2)
{
   LZespolona  Wynik;

  Wynik.re = Skl1.re*Skl2.re - Skl2.im*Skl1.im;
  Wynik.im = Skl1.re*Skl2.im + Skl1.im*Skl2.re;
  return Wynik;
}
LZespolona  operator / (LZespolona  Skl1,  double  Skl2)
{
  LZespolona Wynik;
  if(Skl2==0){
    std::cerr<<"NIE DZIEL PRZEZ ZERO";
    exit(1);
  } 
  
  Wynik.re=Skl1.re/Skl2;
  Wynik.im=Skl1.im/Skl2;
  

  return Wynik;
}
LZespolona  Sprzezenie (LZespolona  Skl1)
{
  LZespolona Wynik;
  Wynik.re=Skl1.re;
  Wynik.im=-Skl1.im;
  return Wynik;
}
LZespolona  operator / (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona Wynik=Skl1*Sprzezenie(Skl2)/modul2(Skl2);
  return Wynik;
}
LZespolona Tworz(double Skl1, double Skl2)
{
  LZespolona Wynik;
  Wynik.re=Skl1;
  Wynik.im=Skl2;
  return Wynik;
}

bool operator ==(LZespolona Skl1, LZespolona Skl2)
{
  if(Skl1.im==Skl2.im&&Skl2.re==Skl2.re)
  return true;
  else return false;
}

bool operator !=(LZespolona Skl1, LZespolona Skl2)
{
  if(Skl1.im!=Skl2.im||Skl2.re!=Skl2.re)
  return true;
  else return false;
}