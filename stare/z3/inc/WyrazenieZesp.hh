#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH
#include<iostream>

#include "LZespolona.hh"
/*OPERATOR ODPOWIADAJĄCY ZA ZNAK MIĘDZY LICZBAMI ZESPOLONYMI*/
enum Operator{ Op_Dodaj,Op_Odejmij,Op_Mnoz, Op_Dziel};

class WyrazenieZesp 
{
private:
    LZespolona Skl1;
    Operator Op;
    LZespolona Skl2;
public:
    /*********KONSTRUKTORY***********/

    WyrazenieZesp()=default;                                               //BEZPARAMETRYCZNY
    WyrazenieZesp(LZespolona &Skl3, Operator &Op1, LZespolona &Skl4){ //PARAMETRYCZNY
      Skl1=Skl3;
      Op=Op1;
      Skl2=Skl4;
    };
    /*******************************/



    /**********GETTERY*************/
    const LZespolona& getskl1() const
    {
        return Skl1;
    };

    const Operator & getOP() const
    {
        return Op;
    };

   const LZespolona & getskl2() const
    {
        return Skl2;
    };
    /******************************/

    /**********SETTERY*************/












    
    void setskl1(LZespolona &skl1){
        Skl1=skl1;
    };
    void setop(Operator &Op1){
        Op=Op1;
    };
    void setskl2(LZespolona &skl2){
        Skl2=skl2;
    };
    /******************************/

    /**************Dekonstruktor*************/
    ~WyrazenieZesp()=default;
    /****************************************/

LZespolona Oblicz();
bool WczytajWyrazenieZesp(std::istream &strm);
};




/**********PRZECIĄŻANIA OPERATORÓW PRZESUNIĘCIA BITOWEGO*************/
std::istream & operator>>(std::istream &strm, Operator &Op);
std::ostream & operator<<(std::ostream &strm, const Operator &Op);

std::istream & operator>>(std::istream &strm, WyrazenieZesp &WyrZ);
std::ostream & operator<<(std::ostream &strm, const WyrazenieZesp &WyrZ);
/********************************************************************/
#endif