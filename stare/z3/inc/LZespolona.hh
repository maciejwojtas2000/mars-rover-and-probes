#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include<ios>
#include<cmath>
#include<iostream>



class LZespolona
{
private:
    double re;
    double im;
public:
/**************KONSTRUKTORY**************/  
    LZespolona(){                                 //BEZPARAMETRYCZNY
        re=0;
        im=0;
    };                         
    LZespolona(double &re1,double &im1){          //PARAMETRYCZNY
        re=re1;
        im=im1;
    };                                      
    LZespolona(LZespolona &LZesp)=default;        //KOPIUJĄCY
/****************************************/





/*************DEKONSTRUKTOR**************/
 ~LZespolona()=default;
/****************************************/




/************SETTERY*********************/

void setim(double x)
{
    this->im=x;
}

void setre(double x)
{
    this->re=x;
}



/****************************************/


/***********GETTERY**********************/
    const double &getre()const
    {
        return re;
    }
    const double &getim()const
    {
        return im;
    }
/****************************************/


    double modul2();
    LZespolona operator + (const LZespolona &Skl2)const;
    LZespolona operator - (const LZespolona &Skl2)const;
    LZespolona operator * (const LZespolona &Skl2)const;   
    LZespolona Sprzerzenie();
    LZespolona operator / (const double &Skl2);
    LZespolona operator / (LZespolona &Skl2);
    bool operator == (const LZespolona &Skl2);
   
};

std::ostream & operator << (std::ostream & strm, const LZespolona &Skl1);
std::istream & operator >> (std::istream &strm, LZespolona &Skl1);
#endif