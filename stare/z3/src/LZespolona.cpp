#include "LZespolona.hh"




std::ostream & operator << (std::ostream & strm, const LZespolona &Skl1){
    strm<<"("<<Skl1.getre()<<std::showpos<<Skl1.getim()<<std::noshowpos<<"i)";
  return strm;
}
std::istream & operator >>(std::istream &strm, LZespolona &Z1)
{ double x;
  char znak;
  strm>>znak;
  if(znak !='(')
  {
    strm.setstate(std::ios::failbit);
  }
 
  strm>>x;
  Z1.setre(x);
  strm>>x;
  Z1.setim(x);
  strm>>znak;
  if(znak !='i')
  {
    strm.setstate(std::ios::failbit);
  }
  strm>>znak;
  if(znak !=')')
  {
    strm.setstate(std::ios::failbit);
  }
  
  strm.setstate(std::ios::goodbit);
  return strm;
}


LZespolona LZespolona::operator +(const LZespolona &Skl2)
{
    LZespolona Wynik;
    Wynik.re=Skl2.re+this->re;
    Wynik.im=Skl2.im+this->im;
    return Wynik;
}

LZespolona LZespolona::operator - (const LZespolona &Skl2)
{
    LZespolona Wynik;
    Wynik.re=-Skl2.re+this->re;
    Wynik.im=-Skl2.im+this->im;
    return Wynik;
}

LZespolona LZespolona::operator * (const LZespolona &Skl2)
{
LZespolona  Wynik;
  Wynik.re = this->re*Skl2.re - this->im*Skl2.im;
  Wynik.im = this->re*Skl2.im + this->im*Skl2.re;
  return Wynik;
}


double LZespolona::modul2(){
    double Wynik;
    Wynik=this->im*this->im+this->re*this->im;
    return Wynik;
}
LZespolona LZespolona::Sprzerzenie(){
    LZespolona Wynik;
    Wynik.im=(-this->im);
    Wynik.re=(this->re);
    return Wynik;
}

LZespolona LZespolona::operator / (const double &Skl2)
{
    LZespolona Wynik;
    Wynik.re=(re/Skl2);
    Wynik.im=(this->im/Skl2);
    return Wynik;
}

LZespolona LZespolona::operator / ( LZespolona &Skl2)
{
    LZespolona Wynik;
    LZespolona Skl1;
    Skl1.re=this->re;
    Skl1.im=this->im;
    Wynik=Skl1*Skl2.Sprzerzenie()/Skl2.modul2();
    return Wynik;
}

bool LZespolona::operator==(const LZespolona &Skl2){
    if(this->im==Skl2.im&& this->re==Skl2.re)
    return true;
    else return false;
}
