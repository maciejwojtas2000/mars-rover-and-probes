#include "StatystykaTestu.hh"


void StatystykaTestu::Wyswietl(){
    std::cout<<"Odpowiedziano na"<<" "<<this->dobre+this->zle<<" "<<"odpowiedzi"<<std::endl;
    std::cout<<"Uzyskano"<<" "<<this->dobre<<" "<<"dobrych odpowiedzi."<<std::endl;
    std::cout<<"Uzyskano"<<" "<<this->zle<<"złych odpowiedzi."<<" "<<std::endl;
    std::cout<<"Uzyskano"<<" "<<this->dobre*100/(this->dobre+this->zle)<<"%"<<" "<<"dobrych odpowiedzi"<<std::endl;
}


void StatystykaTestu::Aktualizuj(bool odp){

    if(odp==0) 
    {
        this->zle=this->zle+1;
        std::cout<<"Zaktualizowano złe"<<std::endl;
    }
    else if(odp==1)
    {
    this->dobre=this->dobre+1;
    std::cout<<this->dobre;
    std::cout<<"Zaktualizowano dobre"<<std::endl;
    }
}