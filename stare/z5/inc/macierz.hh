#ifndef Macierz_HH
#define Macierz_HH

#include<iostream>
#include<cmath>
#include"wektor.hh"
#include"rozmiar.h"

template<typename Typ, int ROZMIAR>
class Macierz
{
private:
    Wektor<Typ,ROZMIAR>tabm[ROZMIAR];
public:
    Macierz()
    {
        for(int i=0;i<ROZMIAR;i++)
        {
        for(int j=0;j<ROZMIAR;j++)
        {
            tabm[i][j]=0;
        }
    }
    }
    
    Macierz(const Macierz &M2){
        for(int i=0;i<ROZMIAR;i++){
            tabm[i]=M2.tabm[i];
        }
    }
    const Wektor<Typ,ROZMIAR> &operator[](int i)const{return tabm[i];}   
    const Typ &operator()(int i, int j)const{return tabm[i][j];}  //GET
    Wektor<Typ,ROZMIAR> &operator[](int i){return tabm[i];}              //SET

    Macierz<Typ,ROZMIAR> operator +(const Macierz<Typ,ROZMIAR> &M2)const;
    Macierz<Typ,ROZMIAR> operator -(const Macierz<Typ,ROZMIAR> &M2)const;
    Macierz<Typ,ROZMIAR> operator *(const double &liczba)const;
     Macierz<Typ,ROZMIAR> operator /(const double &liczba)const;
    Macierz<Typ,ROZMIAR> operator *(const Macierz<Typ,ROZMIAR> &M2)const;
    Wektor<Typ,ROZMIAR> operator *(const Wektor<Typ,ROZMIAR> &W2)const;

    Typ wyznacznikgaussa()const;
    void transpozycja();
    Macierz<Typ,ROZMIAR> odwrotnosc()const;
};


template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator + (const Macierz<Typ,ROZMIAR> &M2) const
{
    Macierz<Typ,ROZMIAR>Wynik;
 for(int i=0;i<ROZMIAR;i++)
 {
     for(int j=0;i<ROZMIAR;i++)
     {
         Wynik[i][j]=tabm[i][j]+M2.tabm[i][j];
     }
 }
 return Wynik;   
}

template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator - (const Macierz<Typ,ROZMIAR> &M2) const
{
    Macierz<Wektor<Typ,ROZMIAR>,ROZMIAR>Wynik;
 for(int i=0;i<ROZMIAR;i++)
 {
     for(int j=0;i<ROZMIAR;i++)
     {
         Wynik[i][j]=tabm[i][j]-M2.tabm[i][j];
     }
 }
 return Wynik;   
}

template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator * (const double &liczba) const
{
      Macierz<Wektor<Typ,ROZMIAR>,ROZMIAR>Wynik;
 for(int i=0;i<ROZMIAR;i++)
 {
     for(int j=0;i<ROZMIAR;i++)
     {
         Wynik[i][j]=tabm[i][j]*liczba;
     }
 }
 return Wynik;   
}

template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator / (const double &liczba) const
{
      Macierz<Wektor<Typ,ROZMIAR>,ROZMIAR>Wynik;
 for(int i=0;i<ROZMIAR;i++)
 {
     for(int j=0;i<ROZMIAR;i++)
     {
         Wynik[i][j]=tabm[i][j]/liczba;
     }
 }
 return Wynik;   
}

template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator * (const Macierz<Typ,ROZMIAR> &M2) const
{
     Macierz<Wektor<Typ,ROZMIAR>,ROZMIAR>Wynik;
    for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;j++)
        {
            double tmp=0;
            for(int k=0;k<ROZMIAR;k++)
            {
               tmp=tmp+this->tabm[i][k]*M2.tabm[k][j]; 
               Wynik.tabm[i][j]=tmp;
            }
        }
    }
    return Wynik;
}


template<typename Typ, int ROZMIAR>
 Wektor<Typ,ROZMIAR>  Macierz<Typ,ROZMIAR>::operator *(const Wektor<Typ,ROZMIAR> &W2)const
{
 Wektor<Typ,ROZMIAR>  Wynik;
    for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;j++)
        {
            Wynik[i]=Wynik[i]+tabm[i][j]*W2[j];
        }
    }
    return Wynik;
}

template<typename Typ, int ROZMIAR>
void Macierz<Typ,ROZMIAR>::transpozycja()
{
   Macierz<Typ,ROZMIAR>tmp;

    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }

    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tabm[i][j]=tmp[j][i];
        }
    }
}
template<typename Typ, int ROZMIAR>
Typ Macierz<Typ,ROZMIAR>::wyznacznikgaussa()const
{
    Typ det=1;
    for(int i=0;i<ROZMIAR;i++)
    {
        
        double tmp=tabm[i][i];
        int tmpr=i;
        for(int r=i+1;r<ROZMIAR;r++){
            if(std::abs(tabm[r][i])>std::abs(tmp))
            {
                tmp=tabm[r][i];
                tmpr=r;
            }
        }
        if(tmp==0){
            return 0;
        }
        if(tmpr!=i)
        {
            Wektor<Typ,ROZMIAR> tmpw;
            tmpw=tabm[i];
            tabm[i]=tabm[tmpr];
            tabm[tmpr]=tabm[i];
            det=det*(-1);
        }
        det*=tmp;
        for(int r=i+1;r<ROZMIAR;r++){
            for(int k=i+1;k<ROZMIAR;k++){
                tabm[r][k]=tabm[r][k]-tabm[i][k]/tmp;
            }
        }
    }
    return det;
}
template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::odwrotnosc()const
{
    Macierz<Typ,ROZMIAR> Wynik;
    Macierz<Typ,ROZMIAR> tmp;
      for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }
    Macierz<Typ,ROZMIAR>D;
    Typ wyz=tmp.wyznacznikgaussa();

    if(wyz==0){
        std::cout<<"Wyznacznik równy 0. Nie istnieje macierz odwrotna"<<std::endl;
        exit(2);
    }   

    //Teraz następuje wyliczenie macierzy dopełnień algebraicznych
        for (int i=0;i<ROZMIAR;i++)
        {
            for(int j=0;j<ROZMIAR;j++)
            {
                D[i][j]=tmp(((i+1)%3), ((j+1)%3))   *tmp(((i+2)%3) ,((j+2)%3))   -tmp(((i+2)%3) , ((j+1)%3))   *tmp(((i+1)%3), ((j+2)%3));
            }
        }
    D.transpozycja();

    Wynik=D/wyz;

    return Wynik;
}



template<typename Typ, int ROZMIAR>
std::ostream & operator <<(std::ostream &strm, const Macierz<Typ, ROZMIAR> &M)
{
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){

            strm<<M[i][j]<<" ";
            if(strm.fail()){
                strm.setstate(std::ios::failbit);
            }
        }
        strm<<std::endl;
    }
    return strm;
}

template<typename Typ, int ROZMIAR>
std::istream & operator >>(std::istream &strm, Macierz<Typ, ROZMIAR> &M)
{
      for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;j++)
        {

            strm>>M[i][j];
            if(strm.fail()){
                strm.setstate(std::ios::failbit);
            }
        }
    
    }
    return strm;
}


#endif






//Jak będą błędy to walnij using array<>