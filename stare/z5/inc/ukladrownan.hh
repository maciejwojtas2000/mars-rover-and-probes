#ifndef UKLADROWNAN_HH
#define UKLADROWNAN_HH

#include "ukladrownan.hh"
#include <iostream>
#include "wektor.hh"
#include "macierz.hh"
#include "rozmiar.h"
template<typename Typ, int ROZMIAR>
class UkladRownan
{
private:
    Macierz<Typ,ROZMIAR>A;
    Wektor<Typ,ROZMIAR>B;
public:
    UkladRownan<Typ,ROZMIAR>(){};
    const Macierz<Typ,ROZMIAR> & getA()const{return A;}
    const Wektor<Typ,ROZMIAR> & getB()const{return B;}
    void setA(const Macierz<Typ,ROZMIAR> &A2){this->A=A2;}
    void setB(const Wektor<Typ,ROZMIAR> &B2){this->B=B2;}

    Wektor<Typ,ROZMIAR>Oblicz()const;


};

template<typename Typ, int ROZMIAR>
Wektor<Typ,ROZMIAR> UkladRownan<Typ,ROZMIAR>::Oblicz()const{

Wektor<Typ,ROZMIAR> Wynik;
Macierz<Typ,ROZMIAR>M=A;
M=M.odwrotnosc();
Wynik=M*B;

return Wynik;
}


template<typename Typ, int ROZMIAR>
std::istream &operator >>(std::istream &strm, UkladRownan<Typ,ROZMIAR> &Ukl)
{
    Macierz<Typ,ROZMIAR>M;
    Wektor<Typ,ROZMIAR>W;

    strm>>M;
    M.transpozycja();
    Ukl.setA(M);
    strm>>W;
    Ukl.setB(W);

    if(strm.fail())
    {
        strm.setstate(std::ios::failbit);
        
    }
    return strm;
}


template<typename Typ, int ROZMIAR>
std::ostream &operator << (std::ostream &strm, const UkladRownan<Typ,ROZMIAR> &Ukl)
{
    Macierz<Typ,ROZMIAR>M=Ukl.getA();
    Wektor<Typ,ROZMIAR>W=Ukl.getB();

    strm<<"Macierz"<<std::endl;
    strm<<M;
    strm<<"Wektor wyrazów wolnych"<<std::endl;
    strm<<W;
    if(strm.fail()){
        strm.setstate(std::ios::failbit);
    }
    return strm;
}
#endif