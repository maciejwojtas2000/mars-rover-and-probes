#infdef WEKTOR_HH
#define WEKTOR_HH
#include<iostream>
#include<array>


class Wektor
{
private:
    std::array<double,3>tab;
public:
    Wektor(){
        for(int i,i<3;i++){
            tab[i]=0;
        }
    };
    Wektor::Wektor(double x, double y, double z){
        tab[0]=x;
        tab[1]=y;
        tab[2]=z;
    }
    ~Wektor();
    Wektor(Wektor &W)=default;

    Wektor operator +(Wektor &W1);
    Wektor operator -(Wektor &W1);
    Wektor operator *(Wektor &W1);
    double  (Wektor &W1);

};


#endif