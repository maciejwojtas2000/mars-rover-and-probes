#include "Wektor.hh"

const double & Wektor::operator [](int index) const{
    return tab[index];
}

double & Wektor::operator [](int index){
    return tab[index];
}

Wektor Wektor::operator +(const Wektor &W2)const
{
    Wektor Wynik;
    for(int i;i<ROZMIAR<<i++){
        Wynik.tab[i]=tab[i]+W2.tab[i];
    }
    return Wynik;
}

Wektor Wektor::operator -(const Wektor &W2)const
{
    Wektor Wynik;
    for(int i;i<ROZMIAR<<i++){
        Wynik.tab[i]=tab[i]-W2.tab[i];
    }
    return Wynik;
}

Wektor Wektor::operator *(const double &liczba)const
{
    Wektor Wynik;
    for(int i;i<ROZMIAR<<i++){
        Wynik.tab[i]=tab[i]*liczba;
    }
    return Wynik;
}

double Wektor::IloczynSkalarny(const Wektor &W2){
    double Wynik=0;
    for(int i=0;i<ROZMIAR;i++){
        Wynik+=tab[i]*W2.tab[i];
    }
    return Wynik;
}

double Wektor::IloczynWektorowy(const Wektor &W2){
    Wektor Wynik;
    Wynik.tab[0]=tab[1]*W2.tab[2]-tab[2]*W2.tab[1];
    Wynik.tab[1]=tab[2]*W2.tab[0]-tab[0]*W2.tab[2];
    Wynik.tab[2]=tab[0]*W2.tab[1]-tab[1]*W2.tab[0];
    return Wynik;
}

double Wektor::dlugosc()const{
double Wynik=0;
for(int i=0;i<ROZMIAR;i++){
    Wynik+=tab[i]*tab[i];
}
Wynik=sqrt(Wynik);
return Wynik;
}


bool Wektor::operator ==(const Wektor &W2){
    for(int i;i<ROZMIAR;i++){
        if (tab[i]==W2.tab[i])
        {
            return true;
        }
        else{
            return false;
            break;
        }
        
    }
}
bool Wektor::operator !=(const Wektor &W2){
    for(int i;i<ROZMIAR;i++){
        if (tab[i]!=W2.tab[i])
        {
            return true;
        }
        else{
            return false;
            break;
        }
        
    }
}

std::istream &operator >>(std::istream &strm, Wektor &Wek){
    for(int i=0;i<ROZMIAR;i++){
        strm>>Wek[i];
        if(strm.fail())
        strm.setstate(std::ios::failbit);
    }
    return strm;
}

std::ostream & operator <<(std::ostream &strm,const Wektor &Wek){

for(int i=0;i<ROZMIAR;i++){
    strm<<Wek[i];
    if(strm.fail()){
        strm.setstate(std::ios::failbit);
    }
    return strm;
}

}

