#include "Macierz.hh"

const Wektor &operator[](int index)const{
    return tabm[index];
}
Wektor & operator[](int index){
    return tabm[index];
}
const double operator[][](int i,int j)const{
    return tabm[i][j];
}
double &operator[][](int i,int j){
    return tabm[i][j];
}

Macierz Macierz::operator +(const Macierz &M)const{
    Macierz Wynik;
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;i++)
        Wynik[i][j]=tabm[i][j]+M[i][j];

    }
    return Wynik;
}


Macierz Macierz::operator -(const Macierz &M)const{
    Macierz Wynik;
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;i++){
        Wynik[i][j]=tabm[i][j]-M[i][j];
        }
    }
    return Wynik;
}


Macierz Macierz::operator *(const Macierz &M){


}
Macierz Macierz::operator *(const double &liczba){
 Macierz Wynik;
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;i++){
            Wynik[i][j]=tabm[i][j]*liczba;

}


Wektor Macierz::operator *(const Wektor &W){
    Wektor Wynik;
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            Wynik[i]+=tabm[i][j]*W[i];
        }
    }
}


bool Macierz::operator ==(const Macierz &M){
    for(int i=0;i<ROZMIAR;i++){
            if(tabm[i]==M.tabm[i]){
                return true;
            }
            else{
                return false;
                break;
            }
        }
    }
}

Macierz Macierz::Transponuj(){
Macierz tmp;
for(int i=0;i<ROZMIAR;i++){
tmp[i]=tabm[i];
}
for(int i=0;i<ROZMIAR;i++){
    for(int j=0;j<ROZMIAR;j++){
        tabm[i][j]=tmp[j][i];
    }
}
return tabm;
}

std::istream &operator >>(std::istream &strm,Macierz &M){
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){

            strm>>M[i][j];
            if(strm.fail()){
                strm.setstate(std::ios::failbit);
            }
        }
    }
    return strm;
}

std::ostream &operator <<(std::ostream &strm, const Macierz &M){
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){

            strm<<M[i][j];
            if(strm.fail()){
                strm.setstate(std::ios::failbit);
            }
        }
    }
    return strm;
}



