#ifndef WEKTOR_HH
#define WEKTOR_HH

#include "rozmiar.h"
#include <iostream>
#include <array>
#include <cassert>
#include <cmath>
/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class Wektor {
  private:
  std::array<double,3>tab;

  public:

  Wektor(){
    tab[0]=0; tab[1]=0;tab[2]=0;
  };

  Wektor(double x, double y,double z)
  {
    tab[0]=x; tab[1]=y;tab[2]=z;
  };
  
const double & operator [](int index) const; //GETTER
double & operator[](int index);              //SETTER


Wektor operator +(const Wektor &W2)const;   //zrobione
Wektor operator -(const Wektor &W2)const;   //zrobione
Wektor operator *(const double &liczba)const;//zrobione
double IloczynSkalarny(const Wektor &W2)const;//zrobione
Wektor IloczynWektorowy(const Wektor &W2)const;//zrobione
double dlugosc()const;//zrobione


bool operator ==(const Wektor & W2)const; //zrobione
bool operator !=(const Wektor & W2)const; //zrobione





  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */    
};
std::istream& operator >> (std::istream &Strm, Wektor &Wek);//zrobione
std::ostream& operator << (std::ostream &Strm, const Wektor &Wek);//zrobione

#endif
