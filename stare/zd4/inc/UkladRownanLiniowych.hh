#ifndef UKLADROWNANLINIOWYCH_HH
#define UKLADROWNANLINIOWYCH_HH

#include "UkladRownanLiniowych.hh"
#include <iostream>
#include "Wektor.hh"
#include "Macierz.hh"
#include "rozmiar.h"

class UkladRownanLiniowych
{
private:
    Macierz A;
    Wektor B;
public:
    UkladRownanLiniowych(){};
    const Macierz & getA()const{return A;}
    const Wektor & getB()const{return B;}
    void setA(const Macierz &A2){this->A=A2;}
    void setB(const Wektor &B2){this->B=B2;}
    Wektor Oblicz()const;
    double blad();


};
std::istream &operator >>(std::istream &strm, UkladRownanLiniowych &Ukl);
std::ostream &operator << (std::ostream &strm, const UkladRownanLiniowych &Ukl);

#endif