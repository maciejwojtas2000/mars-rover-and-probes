#ifndef MACIERZ_HH
#define MACIERZ_HH

#include "rozmiar.h"
#include "Wektor.hh"
#include <iostream>
#include <cmath>
#include <algorithm>

class Macierz {
private: 
  std::array<Wektor,3>tabm;


  public:
  
  Macierz(){
    for(int i=0;i<ROZMIAR;i++)
    {
      for(int j=0;j<ROZMIAR;j++){

        tabm[i][j]=0;     
      }
    }
  };

  Macierz(Wektor W[ROZMIAR])
  {
   for(int i=0;i<ROZMIAR;i++)
   {
   tabm[i]=W[i];  
   }
  };



const Wektor & operator[] (int index) const;   //zrobione
Wektor & operator[] (int index);             //ZROBIONE

const double &operator()(int i,int j) const;//ZROBIONE GET
double &operator()(int i,int j);          //ZROBIONE SET



Macierz operator +(const Macierz &M)const;    //zrobiome
Macierz operator -(const Macierz &M)const;    //zrobione

Macierz operator *(const Macierz &M)const;    
Macierz operator *(const double &liczba)const;//zrobione
Wektor operator *(const Wektor &W)const;      //zrobione
Macierz operator /(const double &liczba)const;


bool operator ==(const Macierz &M)const;      //zrobiome
void Transponuj();                         //zrobione
double Gauss()const;                               //popraw
double Wyznacznik()const;
Macierz Odwrotnosc()const;
Macierz Odwrotnosc2()const;
};


std::istream& operator >> (std::istream &Strm, Macierz &Mac);
std::ostream &operator <<(std::ostream &strm, const Macierz &M);



#endif
