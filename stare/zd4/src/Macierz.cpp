#include "Macierz.hh"

const Wektor & Macierz::operator [](int index)const{
    return tabm[index];
}




Wektor & Macierz::operator[](int index)
{
    return tabm[index];
}



const double &Macierz::operator()(int i,int j)const
{
    return tabm[i][j];
}




double & Macierz::operator()(int i,int j){
    return tabm[i][j];
}

Macierz Macierz::operator +(const Macierz &M)const{
    Macierz Wynik;
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;i++)
        Wynik[i][j]=tabm[i][j]+M[i][j];

    }
    return Wynik;
}




Macierz Macierz::operator -(const Macierz &M)const{
    Macierz Wynik;
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;i++){
        Wynik(i,j)=tabm[i][j]-M(i,j);
        }
    }
    return Wynik;
}


Wektor Macierz::operator *(const Wektor &W)const{
    Wektor Wynik;
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            Wynik[i]+=tabm[i][j]*W[j];
        }
    }
    return Wynik;
}

Macierz Macierz::operator *(const double &liczba)const
{
 Macierz Wynik;
    for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;i++)
        {
            Wynik(i,j)=tabm[i][j]*liczba;
        }
    }
    return Wynik;
}


Macierz Macierz::operator * (const Macierz &M) const{
    Macierz Wynik;
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            double tmp=0;
            for(int k=0;k<ROZMIAR;k++){
               tmp=tmp+this->tabm[i][k]*M.tabm[k][j]; 
               Wynik.tabm[i][j]=tmp;
            }
        }
    }
    return Wynik;
}

Macierz Macierz::operator /(const double &liczba)const
{
 Macierz Wynik;
    for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;j++)
        {
            Wynik(i,j)=tabm[i][j]/liczba;
        }
    }
    return Wynik;
}










bool Macierz::operator ==(const Macierz &M)const
{
    for(int i=0;i<ROZMIAR;i++)
    {
            if(tabm[i]==M.tabm[i])
            {
                return true;
            }
            else
            {
                return false;
                break;
            }
    }
    return true;
}

void Macierz::Transponuj()  
{
   Macierz tmp;

    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }

    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tabm[i][j]=tmp[j][i];
        }
    }

}

double Macierz::Wyznacznik()const{

    double Wynik=0;
    Macierz tmp;
      for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }
    Wynik=tmp[0][0]*tmp[1][1]*tmp[2][2];
    Wynik+=tmp[0][1]*tmp[1][2]*tmp[2][0];
    Wynik+=tmp[0][2]*tmp[1][0]*tmp[2][1];

    Wynik-=tmp[2][0]*tmp[1][1]*tmp[0][2];
    Wynik-=tmp[2][1]*tmp[1][2]*tmp[0][0];
    Wynik-=tmp[2][2]*tmp[1][0]*tmp[0][1];

    return Wynik;

}


double Macierz::Gauss()const
{
    double Wynik=1;
    Macierz tmp;
    Wektor tmp1;
      for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }
    for(int i=0;i<ROZMIAR;i++)
    {
        double t1=tmp(i,i);
        int r=i;
        for(int j=i+1;j<ROZMIAR;j++)
        {
            if(std::abs(tmp(j,i)>std::abs(t1))){
                t1=tmp(j,i);
                r=j;
            }
        }

        if(t1==0)
        {
            break;
        }
        if(r!=i)
        {

            tmp1=tmp[i];
            tmp[i]=tmp[r];
            tmp[r]=tmp1;
            t1*=-1;
        }
        Wynik *=t1;

        for(int j=i+1;j<ROZMIAR;j++){
            for(int k=i+1;k<ROZMIAR;k++){
                tmp(j,k)-=tmp(j,i)*tmp(i,k)/t1;
            }
        }
        
    }
        
        return Wynik;
}

Macierz Macierz::Odwrotnosc()
{
    Macierz Wynik;
    Macierz tmp;
      for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }



    Macierz D;
    double wyz=tmp.Wyznacznik();
    if(wyz==0){
        std::cout<<"Wyznacznik równy 0. Nie istnieje macierz odwrotna"<<std::endl;
        exit(2);
    }
    D(0,0)=(tmp(1,1)*tmp(2,2)-tmp(1,2)*tmp(2,1));
    D(0,1)=-(tmp(1,0)*tmp(2,2)-tmp(2,0)*tmp(1,2));
    D(0,2)=(tmp(1,0)*tmp(2,1)-tmp(2,0)*tmp(1,1));

    D(1,0)=-(tmp(0,1)*tmp(2,2)-tmp(2,1)*tmp(0,2));
    D(1,1)=(tmp(0,0)*tmp(2,2)-tmp(2,0)*tmp(0,2));
    D(1,2)=-(tmp(0,0)*tmp(2,1)-tmp(2,0)*tmp(0,1));

    D(2,0)=(tmp(0,1)*tmp(1,2)-tmp(1,1)*tmp(0,2));
    D(2,1)=-(tmp(0,0)*tmp(1,2)-tmp(1,0)*tmp(0,2));
    D(2,2)=(tmp(0,0)*tmp(1,1)-tmp(1,0)*tmp(0,1));

    D.Transponuj();

    Wynik=D/wyz;
 
    return Wynik;
}

Macierz Macierz::Odwrotnosc2()
{
    Macierz Wynik;
    Macierz tmp;
      for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }
    Macierz D;
    double wyz=tmp.Wyznacznik();

    if(wyz==0){
        std::cout<<"Wyznacznik równy 0. Nie istnieje macierz odwrotna"<<std::endl;
        exit(2);
    }   

    //Teraz następuje wyliczenie macierzy dopełnień algebraicznych
        for (int i=0;i<ROZMIAR;i++)
        {
            for(int j=0;j<ROZMIAR;j++)
            {
                D(i,j)=tmp(((i+1)%3),((j+1)%3))*tmp(((i+2)%3),((j+2)%3))-tmp(((i+2)%3),((j+1)%3))*tmp(((i+1)%3),((j+2)%3));
            }
        }
    D.Transponuj();

    Wynik=D/wyz;

    return Wynik;
}





std::istream &operator >>(std::istream &strm,Macierz &M){
    for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;j++)
        {

            strm>>M[i][j];
            if(strm.fail()){
                strm.setstate(std::ios::failbit);
            }
        }
    
    }
    return strm;
}


std::ostream &operator <<(std::ostream &strm, const Macierz &M){
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){

            strm<<M[i][j]<<" ";
            if(strm.fail()){
                strm.setstate(std::ios::failbit);
            }
        }
        strm<<std::endl;
    }
    return strm;
}

