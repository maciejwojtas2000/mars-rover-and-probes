#include "UkladRownanLiniowych.hh"

Wektor UkladRownanLiniowych::Oblicz()const
{
    Wektor Wynik;
    Macierz M=A;
    M=M.Odwrotnosc2();
    Wynik=M*B;
    return Wynik;
}

double UkladRownanLiniowych::blad()
{
    double Wynik;
    Macierz M=A;
    
    Wektor Wynik1=Oblicz();
    Wektor blad=M*Wynik1-B;
    
    Wynik=blad.dlugosc();
    return Wynik;
    
}
std::istream &operator >> (std::istream &strm, UkladRownanLiniowych &Ukl)
{
    Macierz M;
    Wektor W;

    strm>>M;
    M.Transponuj();
    Ukl.setA(M);
    strm>>W;
    Ukl.setB(W);

    if(strm.fail())
    {
        strm.setstate(std::ios::failbit);
        
    }
    return strm;
}

std::ostream& operator <<( std::ostream & strm, const UkladRownanLiniowych &Ukl)
{
    Macierz M=Ukl.getA();
    Wektor W=Ukl.getB();

    strm<<"Macierz"<<std::endl;
    strm<<M;
    strm<<"Wektor wyrazów wolnych"<<std::endl;
    strm<<W;
    if(strm.fail()){
        strm.setstate(std::ios::failbit);
    }
    return strm;
    
}