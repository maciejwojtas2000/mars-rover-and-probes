#include "WyrazenieZesp.hh"
#include "LZespolona.hh"



std::istream &operator>>(std::istream &strm,Operator &Op){
    char znak;
  strm>>znak;
  switch(znak){
    case '+':{ 
      Op=Op_Dodaj;   break;
    }
    case '-':{
       Op=Op_Odejmij; break;
    }
    case '*':{

     Op=Op_Mnoz;    break;
    }
    case '/':{
       Op=Op_Dziel;   break;
    }
    default:  strm.setstate(std::ios::failbit);
  }
  return strm;
}

std::ostream & operator<<(std::ostream &strm, const Operator & Op)
{
  switch(Op){
    case Op_Dodaj: strm<<'+';   break;
    case Op_Odejmij: strm<<'-'; break;
    case Op_Mnoz: strm<<'*';    break;
    case Op_Dziel: strm<<'/';   break;

    default:  strm.setstate(std::ios::failbit);
  }
  return strm;
}



std::ostream & operator <<(std::ostream &strm, const WyrazenieZesp &WyrZ){
    strm<<WyrZ.Skl1<<WyrZ.Op<<WyrZ.Skl2;
    return strm;    
}

std::istream & operator >> (std::istream &strm, WyrazenieZesp &WyrZ){
    strm>>WyrZ.Skl1>>WyrZ.Op>>WyrZ.Skl2;
    return strm;
}


bool WyrazenieZesp::WczytajWyrazenieZesp(std::istream &strm)
{
    strm>>this->Skl1>>this->Op>>this->Skl2;
    if(strm.fail()==true||strm.eof())
    {
    return false;
    }
    else
    {
    return true;
    }
}


LZespolona WyrazenieZesp::Oblicz(){

LZespolona Wynik;
if(this->Op=Op_Dodaj)
    Wynik=this->Skl1+this->Skl2;
if(this->Op=Op_Odejmij)
    Wynik=this->Skl1-this->Skl2;
if(this->Op=Op_Mnoz)
    Wynik=this->Skl1*this->Skl2;
if(this->Op=Op_Dziel)
    Wynik=this->Skl1/this->Skl2;
else std::cerr<<"Błędny operator"<<std::endl;
return Wynik;
}