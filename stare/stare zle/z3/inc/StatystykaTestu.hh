#ifndef STATYSTYKATESTU_HH
#define STATYSTYKATESTU_HH
#include<iostream>

class StatystykaTestu
{
private:
    int dobre;
    int zle; 
public:

/****************KONSTRUKTORY****************/
    StatystykaTestu();
    StatystykaTestu(int dobre1, int zle2){
        this->dobre=dobre1;
        this->zle=zle2;
    }
/********************************************/


/***************DEKONSTRUKTOR****************/
    ~StatystykaTestu();

/********************************************/



/*************SETTER**I**WYSWIETLANIE********/
    void Aktualizuj(bool odp);
    void Wyswietl();
    
};

#endif