#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include<ios>
#include<cmath>
#include<iostream>



class LZespolona
{
private:
    double re;
    double im;
public:
/**************KONSTRUKTORY**************/
    LZespolona();                         //BEZPARAMETRYCZNY
    LZespolona(double re1,double im1);    //PARAMETRYCZNY
    LZespolona(LZespolona &LZesp);        //KOPIUJĄCY
/****************************************/





/*************DEKONSTRUKTOR**************/
    ~LZespolona();
/****************************************/





/***********GETTERY**********************/
    double RE(){return this->re;};
    double IM(){return this->im;};
/****************************************/




    LZespolona(LZespolona &LZesp)=default;
    LZespolona(double re1, double im1)
  
    {
        this->re=re1;
        this->im=im1;
    };
  
    double modul2();
    LZespolona operator + (const LZespolona &Skl2);
    LZespolona operator - (const LZespolona &Skl2);
    LZespolona operator * (const LZespolona &Skl2);   
    LZespolona Sprzerzenie();
    LZespolona operator / (const double &Skl2);
    LZespolona operator / (const LZespolona &Skl2);
    bool operator == (const LZespolona &Skl2);
   
};

std::ostream & operator << (std::ostream & strm, const LZespolona &Skl1);
std::istream & operator >> (std::istream &strm, LZespolona Skl1);
#endif