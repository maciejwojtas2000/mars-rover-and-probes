#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH
#include<iostream>

#include "LZespolona.hh"
/*OPERATOR ODPOWIADAJĄCY ZA ZNAK MIĘDZY LICZBAMI ZESPOLONYMI*/
enum Operator{ Op_Dodaj,Op_Odejmij,Op_Mnoz, Op_Dziel};

class WyrazenieZesp 
{
private:
    LZespolona Skl1;
    Operator Op;
    LZespolona Skl2;
public:
    /*********KONSTRUKTORY***********/

    WyrazenieZesp();                                               //BEZPARAMETRYCZNY
    WyrazenieZesp(LZespolona Skl3, Operator Op1, LZespolona Skl4){ //PARAMETRYCZNY
    //  this->Skl1=Skl3;
    //  this->Op=Op1;
    //  this->Skl2=Skl4;
    };
    /*******************************/



    /**********GETTERY*************/
    LZespolona getskl1() const; 
    Operator getOP() const;
    LZespolona getskl2() const;
    /******************************/

    /**********SETTERY*************/
    LZespolona setskl1(LZespolona Skl1);
    LZespolona setop(Operator Op1);
    LZespolona setskl2(LZespolona Skl2);
    /******************************/

    /**************Dekonstruktor*************/
    ~WyrazenieZesp();
    /****************************************/

LZespolona Oblicz();
bool WczytajWyrazenieZesp(std::istream &strm);
};




/**********PRZECIĄŻANIA OPERATORÓW PRZESUNIĘCIA BITOWEGO*************/
std::istream & operator>>(std::istream &strm, Operator &Op);
std::ostream & operator<<(std::ostream &strm, const Operator &Op);

std::istream & operator>>(std::istream &strm, WyrazenieZesp &WyrZ);
std::ostream & operator<<(std::ostream &strm, const WyrazenieZesp &WyrZ);
/********************************************************************/
#endif