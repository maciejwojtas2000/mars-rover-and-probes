#ifndef WEKTOR_HH
#define WEKTOR_HH

#include "rozmiar.h"
#include <iostream>


/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class Wektor {
  private:
  std::array<double,3>tab;
}
  public:

  Wektor()
  {
    for(int i;i<rozmiar;i++)
    {
      tab[i]=0;
    }
  };
  Wektor(double x, double y,double z)
  {
    Wektor[0]=x; Wektor[1]=y;Wektor[2]=z;
  };
  ~Wektor();
  Wektor(Wektor &W)=default;

Wektor operator +(Wektor &W2);
Wektor operator -(Wektor &W2);
Wektor operator *(Wektor &W2);
double Wektor::IloczynSkalarny(Wektor &W2);
Wektor Wektor::IloczynWektorowy(Wektor &W2);


  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */    
};


/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::istream& operator >> (std::istream &Strm, Wektor &Wek);

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::ostream& operator << (std::ostream &Strm, const Wektor &Wek);

#endif
