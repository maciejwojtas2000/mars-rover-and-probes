var hierarchy =
[
    [ "drawNS::Draw3DAPI", "a00102.html", [
      [ "drawNS::APIGnuPlot3D", "a00094.html", null ]
    ] ],
    [ "Interfejs_probki", "a00106.html", [
      [ "Probka", "a00142.html", null ]
    ] ],
    [ "Interfejs_przeszkody", "a00110.html", [
      [ "Lazik", "a00122.html", [
        [ "SFR", "a00150.html", null ]
      ] ],
      [ "Probka", "a00142.html", null ]
    ] ],
    [ "Interfejs_rysowania", "a00114.html", [
      [ "ObiektGeom", "a00134.html", [
        [ "Lazik", "a00122.html", null ],
        [ "Probka", "a00142.html", null ],
        [ "powierzchnia", "a00138.html", null ]
      ] ],
      [ "scena", "a00146.html", null ]
    ] ],
    [ "Interfejs_SFR", "a00118.html", [
      [ "SFR", "a00150.html", null ]
    ] ],
    [ "Macierz< Typ, ROZMIAR >", "a00126.html", null ],
    [ "Macierz< double, 3 >", "a00126.html", [
      [ "MacierzOb", "a00130.html", null ]
    ] ],
    [ "drawNS::Point3D", "a00098.html", null ],
    [ "UW", "a00154.html", [
      [ "ObiektGeom", "a00134.html", null ]
    ] ],
    [ "Wektor< Typ, ROZMIAR >", "a00158.html", null ],
    [ "Wektor< double, 3 >", "a00158.html", [
      [ "Wektor3D", "a00162.html", null ]
    ] ],
    [ "Wektor< double, ROZMIAR >", "a00158.html", null ]
];