#include <iostream>
#include <fstream>
#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"
#include "Lazik.hh"
#include "scena.hh"
#include "powierzchnia.hh"
//#include "scena.hh"
using drawNS::APIGnuPlot3D;
using drawNS::Point3D;



int Wektor3D::aktualne;
int Wektor3D::wszystkie;

void wait4key() {
  do {
    std::cout << "\n Press a key to continue..." << std::endl;
  } while(std::cin.get() != '\n');
}

void zwrocutworzeoneobiekty()
{
   std::cout<<"Powstało w ogóle:"<<Wektor3D::getwszystkie()<<"wektorów"<<std::endl;
   std::cout<<"Aktualnie powstało:"<<Wektor3D::getaktualne()<<"wektorów"<<std::endl;

}


int main()
{


 int param=15;
   std::shared_ptr<drawNS::Draw3DAPI> rysownik(new APIGnuPlot3D(-param,param,-param,param,-param,param,param));
powierzchnia p(15,7.5);
int idsceny=p.rysuj(rysownik);

scena Scena(rysownik);
Scena.rysuj(rysownik);


/*
std::cout<<"Rozpocznij naciskając ENTER, aby zobaczyć lazika numer 0"<<std::endl;
   wait4key();

   Scena.wypisznazwe(0);
   Scena.pokazlazika(0,rysownik);

   std::cout<<"Rozpocznij naciskając ENTER, aby zobaczyć lazika numer 1"<<std::endl;
   wait4key();
   Scena.wypisznazwe(1);
   Scena.pokazlazika(1,rysownik);
   std::cout<<"Rozpocznij naciskając ENTER, aby zobaczyć lazika numer 2"<<std::endl;
   wait4key();
    Scena.wypisznazwe(2);
   Scena.pokazlazika(2,rysownik);
   std::cout<<"Rozpocznij naciskając ENTER, aby zobaczyć lazika numer 3"<<std::endl;
   wait4key()
   Scena.wypisznazwe(3);
   Scena.pokazlazika(3,rysownik);
   std::cout<<"Rozpocznij naciskając ENTER, aby przejść do wyboru lazika i rozpocząć ruch"<<std::endl;
*/

int ktorydron=0;

while(1)
{
 

   
    std::cout<<std::endl<<std::endl;
    std::cout<<"Co chcesz zrobić"<<std::endl;
    std::cout<<" Wyswietlic powstale wektory------------------p"<<std::endl;
    std::cout<<" Pokazanie łazików----------------------------w"<<std::endl;
    std::cout<<" Jazda do przodu------------------------------j"<<std::endl;
    std::cout<<" Obracanie się w osi z------------------------o"<<std::endl;
    std::cout<<" Wyjdź----------------------------------------q"<<std::endl;
    std::cout<<" Wyświetl informacje o pobranych próbkach-----i"<<std::endl;
    std::cout<<" Zmiana łazik---------------------------------z"<<std::endl<<std::endl<<std::endl<<std::endl;
    std::cout<<" Aktywny łazik--------------------------------:"<<ktorydron<<std::endl<<std::endl;Scena.wypisznazwe(ktorydron);
    char wybor;
    std::cin>>wybor;
    if(wybor!='w'&&wybor!='o'&&wybor!='j'&&wybor!='p'&&wybor!='z'&&wybor!='q'&&wybor!='i')
    {
       std::cerr<<"Podano złą operację"<<std::endl;
    }else
    {
    switch(wybor)
    { 
case 'i':

Scena.wypiszprobki(ktorydron);
break;


case 'q':
return 0;



       case 'z':
       {
std::cout<<std::endl<<std::endl<<std::endl<<"Wybierz drona[0,1,2,3]"<<std::endl<<std::endl;
std::cin>>ktorydron;
if(ktorydron!=1&&ktorydron!=0&&ktorydron!=2&&ktorydron!=3)
{
   std::cerr<<"Podano złego drona ";
       }
       break;
       }

       
   case 'w':
    {
       std::cout<<"Rozpocznij naciskając ENTER, aby zobaczyć lazika numer 0"<<std::endl;
   wait4key();

   Scena.wypisznazwe(0);
   Scena.pokazlazika(0,rysownik);

   std::cout<<"Rozpocznij naciskając ENTER, aby zobaczyć lazika numer 1"<<std::endl;
   wait4key();
   Scena.wypisznazwe(1);
   Scena.pokazlazika(1,rysownik);
   std::cout<<"Rozpocznij naciskając ENTER, aby zobaczyć lazika numer 2"<<std::endl;
   wait4key();
   Scena.wypisznazwe(2);
   Scena.pokazlazika(2,rysownik);
   std::cout<<"Rozpocznij naciskając ENTER, aby zobaczyć lazika numer 3"<<std::endl;
   wait4key();
   Scena.wypisznazwe(3);
   Scena.pokazlazika(3,rysownik);
   std::cout<<"Rozpocznij naciskając ENTER, aby przejść do wyboru lazika i rozpocząć ruch"<<std::endl;
   break;
   }





      case 'j': {

      double a;
      std::cout<<std::endl<<std::endl;
      std::cout<<"O ile chcesz jechać:"<<std::endl<<std::endl;
      std::cin>>a;

      uint x=Scena.lazikjedz1(ktorydron,a,rysownik);
      break;
      }




      case 'p':
      {
         zwrocutworzeoneobiekty();
         break;
      }





      case 'o':
      {
         uint a;
         std::cout<<std::endl<<std::endl;
        std::cout<<"O ile chcesz się obrócić:(podaj w stopniach)"<<std::endl;
        std::cin>>a;
        Scena.lazikobrot(ktorydron,a,rysownik);
        break;
      }




     default: break;




    }
}
}
wait4key();

return 0;

}
