#ifndef UW_HH
#define UW_HH

#include <vector>
#include "wektor.hh"
#include "macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"
using drawNS::APIGnuPlot3D;
using drawNS::Point3D;


class UW{

protected:
UW *rodzic;
MacierzOb orientacja;
Wektor3D polozenie;



public:
UW(){};
void rotacja(MacierzOb nowa){orientacja=nowa*orientacja;}
void translacja(Wektor3D nowy){polozenie=polozenie+nowy;}
MacierzOb get_orientacja(){return orientacja;}
Wektor3D get_polozenie(){return polozenie;}

Wektor3D przelicz_do_rodzica(Wektor3D punkt)
{
    Wektor3D Wynik=polozenie+orientacja*punkt;
    return Wynik; 
}

void obrotZ(double kat)
  {
    MacierzOb ob;
    ob.MacierzObrotuZ(kat);
    orientacja=ob*orientacja;
  }

void obrotX(double kat)
  {
    MacierzOb ob;
    ob.MacierzObrotuX(kat);
    orientacja=ob*orientacja;
  }


};
 

#endif
