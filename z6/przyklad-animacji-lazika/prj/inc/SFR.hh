#ifndef SFR_HH
#define SFR_HH

#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"
#include "ObiektGeom.hh"
#include "InterfejsSFR.hh"

using namespace std;
using drawNS::APIGnuPlot3D;
using drawNS::Point3D;


class SFR: public Lazik,public Interfejs_SFR
{
protected:
std::vector<std::shared_ptr<Interfejs_probki>>probki_pobrane;
public:

SFR(){};
SFR(Wektor3D W,MacierzOb mac, string n)// : Lazik(W,mac,n) {}
{   
    double y=3;
    double x=2;
    setwysokosc(x);
     setdlugosc(y);
   setszerokosc(x);
       polozenie=W;
    orientacja=mac;
    nazwaObiektu=n;
    aktualizujobiekt();
} 

int rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override{
 uint a;
 rysownik->change_ref_time_ms(0);
 a=rysownik->draw_polyhedron(vector<vector<Point3D> > {{
	tabb[0].zwrocpunkt(), tabb[1].zwrocpunkt(), tabb[2].zwrocpunkt(),tabb[3].zwrocpunkt()
      },{
	tabb[4].zwrocpunkt(), tabb[6].zwrocpunkt(), tabb[7].zwrocpunkt(),tabb[5].zwrocpunkt()       
	  }},"green");
   id=a;
   return id;
}


  void pobranieprobki(std::shared_ptr<Interfejs_probki> probka)override
  {
  probki_pobrane.push_back(probka);
  }


void wyswietlpobraneprobki()override
  {
      if(probki_pobrane.size()!=0)
      {
      


    for(int x=0;x<probki_pobrane.size();x++)
    {
      
      std::cout<<"Próbka nr: "<<x+1<<std::endl;
      probki_pobrane[x]->wypiszprobke();          
    }
  
  }
  else{
    std::cout<<"Pusty zbiornik z próbkami"<<std::endl;
  }
  }
  uint getidprzeszkody()
 {
   this->getid();
 }

};

#endif