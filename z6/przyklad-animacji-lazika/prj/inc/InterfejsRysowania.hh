#ifndef INTERFEJS_RYSOWANIA_HH
#define INTEFEJS_RYSOWANIA_HH

#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"

using drawNS::APIGnuPlot3D;
using drawNS::Point3D;

class Interfejs_rysowania{

public:
virtual int rysuj(std::shared_ptr<drawNS::Draw3DAPI>rysownik)=0;

};
#endif