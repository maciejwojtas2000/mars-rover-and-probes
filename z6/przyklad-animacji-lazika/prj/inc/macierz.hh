#ifndef Macierz_HH
#define Macierz_HH

#include<iostream>
#include<cmath>
#include"wektor.hh"
#include"rozmiar.h"

template<typename Typ, int ROZMIAR>
class Macierz
{
protected:
    Wektor<Typ,ROZMIAR>tabm[ROZMIAR];
public:
    Macierz()
    {
        for(int i=0;i<ROZMIAR;i++)
        {
        for(int j=0;j<ROZMIAR;j++)
        {
            tabm[i][j]=0;
        }
    }
    }
    
    Macierz(const Macierz &M2){
        for(int i=0;i<ROZMIAR;i++){
            tabm[i]=M2.tabm[i];
        }
    }
    const Wektor<Typ,ROZMIAR> &operator[](int i)const{return tabm[i];}   
    const Typ &operator()(int i, int j)const{return tabm[i][j];}  //GET
    Wektor<Typ,ROZMIAR> &operator[](int i){return tabm[i];}              //SET

    Macierz<Typ,ROZMIAR> operator +(const Macierz<Typ,ROZMIAR> &M2)const;
    Macierz<Typ,ROZMIAR> operator -(const Macierz<Typ,ROZMIAR> &M2)const;
    Macierz<Typ,ROZMIAR> operator *(const double &liczba)const;
     Macierz<Typ,ROZMIAR> operator /(const double &liczba)const;
    Macierz<Typ,ROZMIAR> operator *(const Macierz<Typ,ROZMIAR> &M2)const;
    Wektor<Typ,ROZMIAR> operator *(const Wektor<Typ,ROZMIAR> &W2)const;

    Typ wyznacznikgaussa()const;
    void transpozycja();
    Macierz<Typ,ROZMIAR> odwrotnosc()const;
};


template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator + (const Macierz<Typ,ROZMIAR> &M2) const
{
    Macierz<Typ,ROZMIAR>Wynik;
 for(int i=0;i<ROZMIAR;i++)
 {
     for(int j=0;i<ROZMIAR;i++)
     {
         Wynik[i][j]=tabm[i][j]+M2.tabm[i][j];
     }
 }
 return Wynik;   
}

template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator - (const Macierz<Typ,ROZMIAR> &M2) const
{
    Macierz<Wektor<Typ,ROZMIAR>,ROZMIAR>Wynik;
 for(int i=0;i<ROZMIAR;i++)
 {
     for(int j=0;i<ROZMIAR;i++)
     {
         Wynik[i][j]=tabm[i][j]-M2.tabm[i][j];
     }
 }
 return Wynik;   
}

template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator * (const double &liczba) const
{
      Macierz<Wektor<Typ,ROZMIAR>,ROZMIAR>Wynik;
 for(int i=0;i<ROZMIAR;i++)
 {
     for(int j=0;i<ROZMIAR;i++)
     {
         Wynik[i][j]=tabm[i][j]*liczba;
     }
 }
 return Wynik;   
}

template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator / (const double &liczba) const
{
      Macierz<Typ,ROZMIAR>Wynik;
 for(int i=0;i<ROZMIAR;i++)
 {
     for(int j=0;i<ROZMIAR;i++)
     {
         Wynik[i][j]=tabm[i][j]/liczba;
     }
 }
 return Wynik;   
}

template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::operator * (const Macierz<Typ,ROZMIAR> &M2) const
{
     Macierz<Typ,ROZMIAR>Wynik;
    for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;j++)
        {
            double tmp=0;
            for(int k=0;k<ROZMIAR;k++)
            {
               tmp=tmp+this->tabm[i][k]*M2.tabm[k][j]; 
               Wynik.tabm[i][j]=tmp;
            }
        }
    }
    return Wynik;
}


template<typename Typ, int ROZMIAR>
 Wektor<Typ,ROZMIAR>  Macierz<Typ,ROZMIAR>::operator *(const Wektor<Typ,ROZMIAR> &W2)const
{
 Wektor<Typ,ROZMIAR> Wynik;
    for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;j++)
        {
            Wynik[i]=Wynik[i]+tabm[i][j]*W2[j];
        }
    }
    return Wynik;
}

template<typename Typ, int ROZMIAR>
void Macierz<Typ,ROZMIAR>::transpozycja()
{
   Macierz<Typ,ROZMIAR>tmp;

    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }

    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tabm[i][j]=tmp[j][i];
        }
    }
}
/*
template<typename Typ, int ROZMIAR>
Typ Macierz<Typ,ROZMIAR>::wyznacznikgaussa()const
{
    Typ det=1;
    for(int i=0;i<ROZMIAR;i++)
    {
        
        double tmp=tabm[i][i];
        int tmpr=i;
        for(int r=i+1;r<ROZMIAR;r++){
            if(std::abs(tabm[r][i])>std::abs(tmp))
            {
                tmp=tabm[r][i];
                tmpr=r;
            }
        }
        if(tmp==0){
            return 0;
        }
        if(tmpr!=i)
        {
            Wektor<Typ,ROZMIAR> tmpw;
            tmpw=tabm[i];
            tabm[i]=tabm[tmpr];
            tabm[tmpr]=tabm[i];
            det=det*(-1);
        }
        det*=tmp;
        for(int r=i+1;r<ROZMIAR;r++){
            for(int k=i+1;k<ROZMIAR;k++){
                tabm[r][k]=tabm[r][k]-tabm[i][k]/tmp;
            }
        }
    }
    return det;
}*/
template<typename Typ, int ROZMIAR>
Macierz<Typ,ROZMIAR> Macierz<Typ,ROZMIAR>::odwrotnosc()const
{
    Macierz<Typ,ROZMIAR> Wynik;
    Macierz<Typ,ROZMIAR> tmp;
      for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){
            tmp[i][j]=tabm[i][j];
        }
    }
    Macierz<Typ,ROZMIAR>D;
    Typ wyz=tmp.wyznacznikgaussa();

    if(wyz==0){
        std::cout<<"Wyznacznik równy 0. Nie istnieje macierz odwrotna"<<std::endl;
        exit(2);
    }   

    //Teraz następuje wyliczenie macierzy dopełnień algebraicznych
        for (int i=0;i<ROZMIAR;i++)
        {
            for(int j=0;j<ROZMIAR;j++)
            {
                D[i][j]=tmp(((i+1)%3), ((j+1)%3))   *tmp(((i+2)%3) ,((j+2)%3))   -tmp(((i+2)%3) , ((j+1)%3))   *tmp(((i+1)%3), ((j+2)%3));
            }
        }
    D.transpozycja();

    Wynik=D/wyz;

    return Wynik;
}



template<typename Typ, int ROZMIAR>
std::ostream & operator <<(std::ostream &strm, const Macierz<Typ, ROZMIAR> &M)
{
    for(int i=0;i<ROZMIAR;i++){
        for(int j=0;j<ROZMIAR;j++){

            strm<<M[i][j]<<" ";
            if(strm.fail()){
                strm.setstate(std::ios::failbit);
            }
        }
        strm<<std::endl;
    }
    return strm;
}

template<typename Typ, int ROZMIAR>
std::istream & operator >>(std::istream &strm, Macierz<Typ, ROZMIAR> &M)
{
      for(int i=0;i<ROZMIAR;i++)
    {
        for(int j=0;j<ROZMIAR;j++)
        {

            strm>>M[i][j];
            if(strm.fail()){
                strm.setstate(std::ios::failbit);
            }
        }
    
    }
    return strm;
}


class MacierzOb:public Macierz<double,3>
{
public:
MacierzOb(const Macierz<double,3>&M):Macierz<double,3>(M){};
MacierzOb()
 {
     tabm[0][0]=1;
     tabm[0][1]=0;
     tabm[1][0]=0;
     tabm[1][1]=1;
     tabm[2][0]=0;
     tabm[0][2]=0;
     tabm[2][1]=0;
     tabm[1][2]=0;
     tabm[2][2]=1;
 }
MacierzOb(int tab[9]){
        {
     tabm[0][0]=tab[0];
     tabm[0][1]=tab[1];
     tabm[1][0]=tab[2];
     tabm[1][1]=tab[3];
     tabm[2][0]=tab[4];
     tabm[0][2]=tab[5];
     tabm[2][1]=tab[6];
     tabm[1][2]=tab[7];
     tabm[2][2]=tab[8];
        }

}
 void MacierzObrotuX(double kat){
     double rad=(kat*M_PI)/180;
     tabm[1][1]=cos(rad);
     tabm[2][2]=cos(rad);

     tabm[1][2]=-sin(rad);
     tabm[2][1]=sin(rad);
 }    

 void MacierzObrotuZ(double kat){
     double rad=(kat*M_PI)/180;
     tabm[0][0]=cos(rad);
     tabm[1][1]=cos(rad);

     tabm[0][1]=-sin(rad);
     tabm[1][0]=sin(rad);
 }
};
#endif






//Jak będą błędy to walnij using array<>