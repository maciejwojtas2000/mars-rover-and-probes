#ifndef OBIEKTGEOM_HH
#define OBIEKTGEOM_HH

#include <vector>
#include <cmath>
#include "wektor.hh"
#include "macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"
#include "UW.hh"
#include <chrono>
#include <thread>
#include "InterfejsRysowania.hh"

using namespace std;
using drawNS::APIGnuPlot3D;
using drawNS::Point3D;








class ObiektGeom:public UW, public Interfejs_rysowania{
 
  protected:
  std::string nazwaObiektu;
  std::array<Wektor3D,8>tabb;
  string color;

  uint id;
  double dlugosc=2;
  double szerokosc=2;
  double wysokosc=2;

  
  public:

  double promien()
  {
    double x=sqrt((dlugosc/2*dlugosc/2)+(szerokosc/2*szerokosc/2)+(wysokosc/2*wysokosc/2));
    return x;
  }
ObiektGeom(){};

  void setdlugosc(double &w)
  {dlugosc=w;
  
  }

void setszerokosc(double &w)
  {szerokosc=w;
  
  }
void setwysokosc(double &w)
  {wysokosc=w;
  
  }

    uint getid(){
      return id;
    }

  ObiektGeom(Wektor3D trans, MacierzOb orientacja1)
  {
    
    for(int i=0;i<8;i++){
      tabb[i]=Wektor3D(0,0,0);
    }

    

    

    polozenie=trans;
    orientacja=orientacja1; 

    tabb[0]=orientacja*Wektor3D(-dlugosc/2,szerokosc/2,-wysokosc/2)+polozenie;
    tabb[1]=orientacja*Wektor3D(dlugosc/2,szerokosc/2,-wysokosc/2)+polozenie;
    tabb[2]=orientacja*Wektor3D(dlugosc/2,-szerokosc/2,-wysokosc/2)+polozenie;
    tabb[3]=orientacja*Wektor3D(-dlugosc/2,-szerokosc/2,-wysokosc/2)+polozenie;
    tabb[4]=orientacja*Wektor3D(-dlugosc/2,szerokosc/2,wysokosc/2)+polozenie;
    tabb[5]=orientacja*Wektor3D(-dlugosc/2,-szerokosc/2,wysokosc/2)+polozenie;
    tabb[6]=orientacja*Wektor3D(dlugosc/2,szerokosc/2,wysokosc/2)+polozenie;
    tabb[7]=orientacja*Wektor3D(dlugosc/2,-szerokosc/2,wysokosc/2)+polozenie;
/*
    tabb[0]=orientacja*polozenie+Wektor3D(-dlugosc/2,szerokosc/2,-wysokosc/2);
    tabb[1]=orientacja*polozenie+Wektor3D(dlugosc/2,szerokosc/2,-wysokosc/2);
    tabb[2]=orientacja*polozenie+Wektor3D(dlugosc/2,-szerokosc/2,-wysokosc/2);
    tabb[3]=orientacja*polozenie+Wektor3D(-dlugosc/2,-szerokosc/2,-wysokosc/2);
    tabb[4]=orientacja*polozenie+Wektor3D(-dlugosc/2,szerokosc/2,wysokosc/2);
    tabb[5]=orientacja*polozenie+Wektor3D(-dlugosc/2,-szerokosc/2,wysokosc/2);
    tabb[6]=orientacja*polozenie+Wektor3D(dlugosc/2,szerokosc/2,wysokosc/2);
    tabb[7]=orientacja*polozenie+Wektor3D(dlugosc/2,-szerokosc/2,wysokosc/2);
*/
  }
void aktualizujobiekt()
  {
    tabb[0]=orientacja*Wektor3D(-dlugosc/2,szerokosc/2,-wysokosc/2)+polozenie;
    tabb[1]=orientacja*Wektor3D(dlugosc/2,szerokosc/2,-wysokosc/2)+polozenie;
    tabb[2]=orientacja*Wektor3D(dlugosc/2,-szerokosc/2,-wysokosc/2)+polozenie;
    tabb[3]=orientacja*Wektor3D(-dlugosc/2,-szerokosc/2,-wysokosc/2)+polozenie;
    tabb[4]=orientacja*Wektor3D(-dlugosc/2,szerokosc/2,wysokosc/2)+polozenie;
    tabb[5]=orientacja*Wektor3D(-dlugosc/2,-szerokosc/2,wysokosc/2)+polozenie;
    tabb[6]=orientacja*Wektor3D(dlugosc/2,szerokosc/2,wysokosc/2)+polozenie;
    tabb[7]=orientacja*Wektor3D(dlugosc/2,-szerokosc/2,wysokosc/2)+polozenie;

  }

  void aktualizujobiekt2()
  {
    tabb[0]=orientacja*tabb[0]+polozenie;
    tabb[1]=orientacja*tabb[1]+polozenie;
    tabb[2]=orientacja*tabb[2]+polozenie;
    tabb[3]=orientacja*tabb[3]+polozenie;
    tabb[4]=orientacja*tabb[4]+polozenie;
    tabb[5]=orientacja*tabb[5]+polozenie;
    tabb[6]=orientacja*tabb[6]+polozenie;
    tabb[7]=orientacja*tabb[7]+polozenie;
  }
  void przesuniecie(const Wektor3D &W)
  {
    polozenie=polozenie+orientacja*W;

  }
	
  void obrotZ(double kat)
  {
    MacierzOb ob;
    ob.MacierzObrotuZ(kat);
    orientacja=ob*orientacja;
  }

  void obrotX(double kat)
  {
    MacierzOb ob;
    ob.MacierzObrotuX(kat);
    orientacja=ob*orientacja;
  }



void wait4key() {
  do {
    std::cout << "\n Press a key to continue..." << std::endl;
  } while(std::cin.get() != '\n');
}



 int rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override{
 uint a;
 rysownik->change_ref_time_ms(0);
 a=rysownik->draw_polyhedron(vector<vector<Point3D> > {{
	tabb[0].zwrocpunkt(), tabb[1].zwrocpunkt(), tabb[2].zwrocpunkt(),tabb[3].zwrocpunkt()
      },{
	tabb[4].zwrocpunkt(), tabb[6].zwrocpunkt(), tabb[7].zwrocpunkt(),tabb[5].zwrocpunkt()       
	  }},"blue");
   id=a;
   return id;
   
   
 //  wait4key();
}
  //Lazik Lazik1(1,1,1,0);
  void eraseall(std::shared_ptr<drawNS::Draw3DAPI> rysownik)
  {//for(int j=1;j<(k+1);j++){
    for(int i=0;i<12;i++){
    rysownik->erase_shape(i);
    }
  //}
  }




 uint animacjajedz(double len,std::shared_ptr<drawNS::Draw3DAPI> rysownik)//jeszcze kolor
  { 
    rysownik->change_ref_time_ms(0);
    rysownik->erase_shape(id);
    double i=0;
    while(i<len)
    {
    rysuj(rysownik);

    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    przesuniecie(Wektor3D(0.1,0,0));
    aktualizujobiekt();
    rysownik->erase_shape(id);
    rysownik->redraw();
    i=i+0.1;

    }
    rysuj(rysownik);
    return id;
  }

  string getnazwa()
{
    
    return nazwaObiektu;
}

 uint animacjaobrotz(int kat,std::shared_ptr<drawNS::Draw3DAPI> rysownik)
  {
    rysownik->erase_shape(id);
    rysownik->change_ref_time_ms(0);
    int i=0;

    if(kat>0)
    {
      while(i<kat)
      {
      rysuj(rysownik);
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
      obrotZ(1);
      aktualizujobiekt();
      rysownik->erase_shape(id);
      rysownik->redraw();
      i++;  
      }
    
    }
    else
    {
        while(i<std::abs(kat))
      {
      rysuj(rysownik);

      std::this_thread::sleep_for(std::chrono::milliseconds(20));
      obrotZ(-1);
      aktualizujobiekt();
      rysownik->erase_shape(id);
      rysownik->redraw();
      i++;
      }
    }
    rysuj(rysownik);
    return id;
  }

void wypiszlazika(){
  for(int i=0;i<8;i++){
  std::cout<<tabb[i]<<std::endl;
}
}

};


#endif
