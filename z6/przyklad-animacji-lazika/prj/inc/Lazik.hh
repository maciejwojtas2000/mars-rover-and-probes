#ifndef LAZIK_HH
#define LAZIK_HH

#include <vector>
#include "wektor.hh"
#include "macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"
#include "ObiektGeom.hh"
#include "InterfejsPrzeszkody.hh"
using drawNS::APIGnuPlot3D;
using drawNS::Point3D;


class Lazik:public Interfejs_przeszkody,public ObiektGeom
{
protected:

public:
Lazik(){};
Lazik(Wektor3D W, MacierzOb mac, string n){
    polozenie=W;

    orientacja=mac;
    nazwaObiektu=n;
    aktualizujobiekt();
}

 
  bool czykolizja1(shared_ptr<Lazik> &lazik1)override{
    
    double r1=lazik1->promien();
    double r2=this->promien();
    Wektor3D w1=lazik1->get_polozenie();
    Wektor3D w2=this->get_polozenie();
    Wektor3D roznica=w1-w2;
    if(roznica.dlugosc()>(r1+r2)){
      return 0;
    }
    else
      {
      return 1;
      }
    }
    uint getidprzeszkody()
 {
   this->getid();
 }

};


#endif
