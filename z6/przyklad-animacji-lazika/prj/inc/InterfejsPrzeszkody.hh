#ifndef INTERFEJS_PRZESZKODY_HH
#define INTERFEJS_PRZESZKODY_HH

class Lazik;

using drawNS::APIGnuPlot3D;
using drawNS::Point3D;

class Interfejs_przeszkody
{
    public:
    virtual bool czykolizja1(shared_ptr<Lazik> &lazik1)=0; //nie ma 0, kolizja 1
    virtual uint getidprzeszkody()=0;
};
#endif