#ifndef WEKTOR_HH
#define WEKTOR_HH

#include <iostream>
#include <cmath>
#include <array>
#include"rozmiar.h"
#include"Dr3D_gnuplot_api.hh"
#include"Draw3D_api_interface.hh"


using drawNS::Point3D;
using drawNS::APIGnuPlot3D;


template<typename Typ, int ROZMIAR>

class Wektor
{
protected:
    std::array<Typ, ROZMIAR> tab;
 
public:
    Wektor(){
        for(int i=0;i<ROZMIAR;i++)
        {
            tab[i]=0;
        }
    }    

    const Typ &operator[](int i)const{return tab[i];}//GET
    Typ &operator[](int i){return tab[i];} //SET


    Wektor<Typ, ROZMIAR> operator +(const Wektor<Typ,ROZMIAR> &W2)const; //ZROBIONE
    Wektor<Typ, ROZMIAR> operator *(const double &liczba)const; //ZROBIONE
    Wektor<Typ, ROZMIAR> operator -(const Wektor<Typ,ROZMIAR> &W2)const; //ZROBIONE
    Wektor<Typ, ROZMIAR> IloczynWektorowy(const Wektor<Typ,ROZMIAR> &W2)const; 
    double IloczynSkalarny(const Wektor<Typ,ROZMIAR> &W2)const; //ZROBIUONE
    double dlugosc()const;                                       //ZROBIONE
    bool operator ==(Wektor<Typ, ROZMIAR>&W)const;              //ZROBIUONE
    


};

template<typename Typ, int ROZMIAR>
bool Wektor<Typ,ROZMIAR>::operator==(Wektor<Typ,ROZMIAR>&W2)const{
for(int i=0;i<ROZMIAR;i++)
    {
        if(tab[i]==W2.tab[i])
        {
            return true;
        }
        else
        {
            return false;
            break;
        }
    }
}


template<typename Typ, int ROZMIAR>
Wektor<Typ, ROZMIAR> Wektor<Typ,ROZMIAR>::operator + (const Wektor<Typ,ROZMIAR> &W2)const{
Wektor<Typ,ROZMIAR>Wynik;
for(int i=0;i<ROZMIAR;i++){
 Wynik[i]=tab[i]+W2.tab[i];
    }
return Wynik;
}

template<typename Typ, int ROZMIAR>
Wektor<Typ, ROZMIAR> Wektor<Typ,ROZMIAR>::operator - (const Wektor<Typ,ROZMIAR> &W2)const{

Wektor<Typ,ROZMIAR>Wynik;
for(int i=0;i<ROZMIAR;i++){
 Wynik[i]=tab[i]-W2.tab[i];
    }
return Wynik;
}


template<typename Typ, int ROZMIAR>
Wektor<Typ, ROZMIAR> Wektor<Typ,ROZMIAR>::operator * (const double &liczba)const{

Wektor<Typ,ROZMIAR>Wynik;
for(int i=0;i<ROZMIAR;i++){
 Wynik[i]=tab[i]*liczba;
    }
return Wynik;
}





template<typename Typ, int ROZMIAR>
double Wektor<Typ,ROZMIAR>::IloczynSkalarny(const Wektor<Typ,ROZMIAR> &W2)const
{
    double Wynik;

for(int i=0; i<ROZMIAR;i++)
    {
    Wynik+=tab[i]*W2.tab[2];
    }
    return Wynik;
}

template<typename Typ,int ROZMIAR>
double Wektor<Typ,ROZMIAR>::dlugosc()const{
    double Wynik=0;
    for(int i=0;i<ROZMIAR;i++){
        Wynik+=tab[i]*tab[i];
    }
    Wynik=sqrt(Wynik);
    return Wynik;
}




template<typename Typ, int ROZMIAR>
std::ostream & operator <<(std::ostream &strm, const Wektor<Typ, ROZMIAR> &W)
{
    for(int i=0;i<ROZMIAR;i++)
    {
        strm<<W[i]<<" ";
    }
    if(strm.fail())
    {
        strm.setstate(std::ios::failbit);
    }
    return strm;
}

template<typename Typ, int ROZMIAR>
std::istream & operator >>(std::istream &strm, Wektor<Typ, ROZMIAR> &W)
{
    for(int i=0;i<ROZMIAR;i++)
    {   
        strm>>W[i];
        if(strm.fail())
        {
            strm.setstate(std::ios::failbit);
        }   
    }
    return strm;
}

class Wektor3D:public Wektor<double,3>
{
    private:
    static int aktualne;
    static int wszystkie;
    public:


    static int getaktualne()
    {   
        int x=aktualne;
        return x;
    }

    static int getwszystkie()
    {
        int x=wszystkie;
        return x;
    }

    const double &operator[](int i)const{return tab[i];}//GET
    double &operator[](int i){return tab[i];} //SET
    Wektor3D(){
        tab[0]=0;
        tab[1]=0;
        tab[2]=0;
        ++aktualne;
        ++wszystkie;
        }
    Wektor3D(const Wektor<double,3>&W):Wektor<double,3>(W){++aktualne;
        ++wszystkie;}
    Wektor3D(double x,double y,double z){
        tab[0]=x;
        tab[1]=y;
        tab[2]=z;
        ++aktualne;
        ++wszystkie;
    }
Wektor3D(const Wektor3D &W)
{
    ++aktualne;
        ++wszystkie;
          tab[0]=W.tab[0];
        tab[1]=W.tab[1];
        tab[2]=W.tab[2];
}
~Wektor3D()
{
    aktualne--;
}

Wektor3D operator=(const Wektor3D W)
{
    ++aktualne;
    ++wszystkie;
  tab[0]=W.tab[0];
        tab[1]=W.tab[1];
        tab[2]=W.tab[2];
}
drawNS::Point3D zwrocpunkt(){

  return drawNS::Point3D(tab[0],tab[1],tab[2]);
 }
/*Wektor3D(const Wektor3D &&W)=delete;

Wektor3D operator=(const Wektor3D &&W){++aktualne;
        ++wszystkie;

};
*/
};
#endif