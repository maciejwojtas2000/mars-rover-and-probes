#ifndef POWIERZCHNIA_HH
#define POWIERZCHNIA_HH

#include "ObiektGeom.hh"
#include "wektor.hh"
#include "macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"

using drawNS::APIGnuPlot3D;
using drawNS::Point3D;
using namespace std;

class powierzchnia: public ObiektGeom
{
    protected:
     Wektor3D punkt[24];
    public:

    powierzchnia(double a,double b)
    {  
       punkt[0]={-a,-a,0};
       punkt[1]={-a,-b,0};
       punkt[2]={-a,0,0};
       punkt[3]={-a,b,0};
       punkt[4]={-a,a,0};
       
       
       punkt[5]={-b,-a,0};
       punkt[6]={-b,-b,0};
       punkt[7]={-b,0,0};
       punkt[8]={-b,b,0};
       punkt[9]={-b,a,0};
       
       punkt[10]={0,-a,0};
       punkt[11]={0,-b,0};
       punkt[12]={0,0,0};
       punkt[13]={0,b,0};
       punkt[14]={0,a,0};
       

       punkt[15]={b,-a,0};
       punkt[16]={b,-b,0};
       punkt[17]={b,0,0};
       punkt[18]={b,b,0};
       punkt[19]={b,a,0};

       punkt[20]={a,-a,0};
       punkt[21]={a,-b,0};
       punkt[22]={a,0,0};
       punkt[23]={a,b,0};
       punkt[24]={a,a,0};
    }
    const Wektor3D getpunkt(int i)const{
        return punkt[i];
    }


    int rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override
    {
        powierzchnia s(*this);
        for(int i=0;i<24;i++)
        {
            s.punkt[i]=punkt[i];
        }
        int x=rysownik->draw_surface(vector<vector<drawNS::Point3D>>{
            {     s.punkt[0].zwrocpunkt(), s.punkt[1].zwrocpunkt(), s.punkt[2].zwrocpunkt(), s.punkt[3].zwrocpunkt(), s.punkt[4].zwrocpunkt()},
            {     s.punkt[5].zwrocpunkt(), s.punkt[6].zwrocpunkt(), s.punkt[7].zwrocpunkt(), s.punkt[8].zwrocpunkt(), s.punkt[9].zwrocpunkt()},
            {     s.punkt[10].zwrocpunkt(), s.punkt[11].zwrocpunkt(), s.punkt[12].zwrocpunkt(), s.punkt[13].zwrocpunkt(), s.punkt[14].zwrocpunkt()},
            {     s.punkt[15].zwrocpunkt(), s.punkt[16].zwrocpunkt(), s.punkt[17].zwrocpunkt(), s.punkt[18].zwrocpunkt(), s.punkt[19].zwrocpunkt()},
            {     s.punkt[20].zwrocpunkt(), s.punkt[21].zwrocpunkt(), s.punkt[22].zwrocpunkt(), s.punkt[23].zwrocpunkt(), s.punkt[24].zwrocpunkt()}
            
            },"red");
            id=x;
        return x;
    }



};





#endif