#ifndef PROBKA_HH
#define PROBKA_HH

#include <vector>
#include "wektor.hh"
#include "macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"
#include "UW.hh"
#include <chrono>
#include <thread>
#include "InterfejsProbki.hh"
#include "ObiektGeom.hh"
#include "InterfejsPrzeszkody.hh"
#include "Lazik.hh"

using namespace std;
using drawNS::APIGnuPlot3D;
using drawNS::Point3D;

double dlugosc=0.5;
double szerokosc=0.25;
double wysokosc=0.25;






class Probka: public Interfejs_przeszkody,public Interfejs_probki, public ObiektGeom
{
protected:

public:

Probka(){};
Probka(Wektor3D W,MacierzOb mac, string n)
{   
    double y=0.5;
    double x=2;
    setwysokosc(y);
     setdlugosc(y);
   setszerokosc(y);
       polozenie=W;
    orientacja=mac;
    nazwaObiektu=n;
    aktualizujobiekt();
} 

int rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override{
 uint a;
 rysownik->change_ref_time_ms(0);
 a=rysownik->draw_polyhedron(vector<vector<Point3D> > {{
	tabb[0].zwrocpunkt(), tabb[1].zwrocpunkt(), tabb[2].zwrocpunkt(),tabb[3].zwrocpunkt()
      },{
	tabb[4].zwrocpunkt(), tabb[6].zwrocpunkt(), tabb[7].zwrocpunkt(),tabb[5].zwrocpunkt()       
	  }},"black");
   id=a;
   return id;
 
 //  wait4key();
 
}
uint getidprzeszkody()
 {
   this->getid();
 }

  bool czykolizja1(shared_ptr<Lazik> &lazik1)override{
    double r1=lazik1->promien();
    double r2=this->promien();
    Wektor3D w1=lazik1->get_polozenie();
    Wektor3D w2=this->get_polozenie();
    Wektor3D roznica=w1-w2;
    if(roznica.dlugosc()>(r1+r2)){
      return 0;
    }else
      {
      return 1;
      }
    }

  bool czymoznazebrac(shared_ptr<Lazik> &lazik1)override{//probka
    double r1=lazik1->promien();
    double r2=this->promien();
    Wektor3D w1=lazik1->get_polozenie();
    Wektor3D w2=this->get_polozenie();
    Wektor3D roznica=w1-w2;
    if(roznica.dlugosc()<r1+r2+1){
      return 1;
    }
    else
    {
      return 0;
    }
    }

    string wypiszprobke()override{
      std::cout<<"nazwa probki"<<this->getnazwa();
      return this->getnazwa();
    }
  
};

#endif