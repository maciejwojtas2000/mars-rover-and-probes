#ifndef INTERFEJS_PROBKI_HH
#define INTERFEJS_PROBKI_HH

class Lazik;

class Interfejs_probki
{
    public:

  virtual bool czymoznazebrac(shared_ptr<Lazik> &lazik1)=0;//1 mozna,0
  virtual string wypiszprobke()=0;
};
#endif