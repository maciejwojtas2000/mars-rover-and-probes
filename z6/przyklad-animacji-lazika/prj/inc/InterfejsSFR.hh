#ifndef INTERFEJS_SFR_HH
#define INTERFEJS_SFR_HH

class Interfejs_probki;
class Probka;
class Interfejs_SFR
{
public:
     virtual void pobranieprobki(shared_ptr<Interfejs_probki>(probka))=0;
     virtual void wyswietlpobraneprobki()=0;
};
#endif