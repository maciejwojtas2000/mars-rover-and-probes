#ifndef scena_HH
#define scena_HH

#include "ObiektGeom.hh"
#include <vector>
#include "wektor.hh"
#include "macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Draw3D_api_interface.hh"
#include "Lazik.hh"
#include "powierzchnia.hh"
#include "Probka.hh"
#include "SFR.hh"
#include "algorithm"
//#include "powierzchnia.hh"


using drawNS::APIGnuPlot3D;
using drawNS::Point3D;
using namespace std;


class scena:public Interfejs_rysowania
{
    private:
    std::vector<std::shared_ptr<Interfejs_rysowania>>kolekcja_rysowalna;
    std::vector<std::shared_ptr<Lazik>>kolekcja_lazikow;
    std::vector<std::shared_ptr<Interfejs_probki>>kolekcja_probek;
     std::vector<std::shared_ptr<Interfejs_przeszkody>>kolekcja_przeszkod;
    public:

    scena(std::shared_ptr<drawNS::Draw3DAPI> rysownik)
    {   MacierzOb Mac;
        Wektor3D W={-5,-5,2};
        Wektor3D W1={-0,-5,2}; 
        Wektor3D W2={-5,0,2};
        Wektor3D W3={-5,3,2};
        Wektor3D P1={0,3,0.5};
        Wektor3D P2={0,10,0.5};
        Wektor3D P3={0,0,0.5};
        string p1="Rogalik1";
        string p2="Rogalik2";
        string p3="Rogalik3";
        string n1="Curiosity";
        string n2="Perseverance";
        string n3="Sojourner";
        string n4="SFR";
        //kolekcja_rysowalna.push_back(make_shared<powierzchnia>(12,6)); //ZAPYTAJ SIĘ O BŁĄÐ Z DZIEDICZENIEM INTERFEJSU RYSOWANIA
        kolekcja_lazikow.push_back(make_shared<Lazik>(W1,Mac,n2));
        kolekcja_lazikow.push_back(make_shared<Lazik>(W,Mac,n3));
        kolekcja_lazikow.push_back(make_shared<SFR>(W3,Mac,n4));
        kolekcja_probek.push_back(make_shared<Probka>(P1,Mac,p1));
        kolekcja_probek.push_back(make_shared<Probka>(P2,Mac,p2));
        kolekcja_probek.push_back(make_shared<Probka>(P3,Mac,p3));

        kolekcja_przeszkod.push_back(dynamic_pointer_cast<Interfejs_przeszkody>(kolekcja_lazikow[0]));
        kolekcja_przeszkod.push_back(dynamic_pointer_cast<Interfejs_przeszkody>(kolekcja_lazikow[1]));
        kolekcja_przeszkod.push_back(dynamic_pointer_cast<Interfejs_przeszkody>(kolekcja_lazikow[2]));
        kolekcja_przeszkod.push_back(dynamic_pointer_cast<Interfejs_przeszkody>(kolekcja_probek[0]));
        kolekcja_przeszkod.push_back(dynamic_pointer_cast<Interfejs_przeszkody>(kolekcja_probek[1]));
        kolekcja_przeszkod.push_back(dynamic_pointer_cast<Interfejs_przeszkody>(kolekcja_probek[2]));


        kolekcja_rysowalna.push_back(dynamic_pointer_cast<Interfejs_rysowania>(kolekcja_lazikow[0]));
        kolekcja_rysowalna.push_back(dynamic_pointer_cast<Interfejs_rysowania>(kolekcja_lazikow[1]));
        kolekcja_rysowalna.push_back(dynamic_pointer_cast<Interfejs_rysowania>(kolekcja_lazikow[2]));
        kolekcja_rysowalna.push_back(dynamic_pointer_cast<Interfejs_rysowania>(kolekcja_probek[0]));
        kolekcja_rysowalna.push_back(dynamic_pointer_cast<Interfejs_rysowania>(kolekcja_probek[1]));
        kolekcja_rysowalna.push_back(dynamic_pointer_cast<Interfejs_rysowania>(kolekcja_probek[2]));
    }

    int rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override
    {
        int id=0;
           for(auto & x:kolekcja_rysowalna){
           id+=x->rysuj(rysownik);
        
         }
    return id;
    }


  void usunprobke(std::shared_ptr<Interfejs_probki> probka)
    {
        cout<<"Następuje próba usunięcia próbki"<<std::endl;
        //shared_ptr<Interfejs_probki>wsk=probka;
        kolekcja_probek.erase(remove(kolekcja_probek.begin(),kolekcja_probek.end(),dynamic_pointer_cast<Interfejs_probki>(probka)),kolekcja_probek.end());
        kolekcja_rysowalna.erase(remove(kolekcja_rysowalna.begin(),kolekcja_rysowalna.end(),dynamic_pointer_cast<Interfejs_rysowania>(probka)),kolekcja_rysowalna.end());
        kolekcja_przeszkod.erase(remove(kolekcja_przeszkod.begin(),kolekcja_przeszkod.end(),dynamic_pointer_cast<Interfejs_przeszkody>(probka)),kolekcja_przeszkod.end());
    }
    
  void wypisanieprobek()
      {
          {
     std::cout<<kolekcja_rysowalna.size();
           for(int x=0;x<kolekcja_probek.size();x++)
           {
           kolekcja_probek[x]->wypiszprobke();
           }
        }
      }


    void pobranieprobki1(shared_ptr<Interfejs_SFR>lazik, shared_ptr<Interfejs_probki> probka)
    {   
        wypisanieprobek();
        lazik->pobranieprobki(probka);
        usunprobke(probka);
    
    }

bool czykolizja(std::shared_ptr<Lazik> &lazik1,shared_ptr<drawNS::Draw3DAPI>rysownik, int idlazika)
{ 
    bool a;
     for(auto & x:kolekcja_przeszkod)
        {
           if(x!=lazik1)
               {
               a=dynamic_pointer_cast<Interfejs_przeszkody>(x)->czykolizja1(lazik1);
               }
            if(a)
                {
                    
                    if(dynamic_pointer_cast<Interfejs_SFR>(lazik1))
                    {
                          if(dynamic_pointer_cast<Interfejs_probki>(x))
                            {
                                kolekcja_lazikow[idlazika]->rysuj(rysownik);
                                std::cout<<"Czy chcesz pobrać próbkę?(t/n)"<<std::endl;
                                char decyzja;
                                std::cin>>decyzja;
                                if(decyzja=='t')
                                  {
                                      rysownik->erase_shape(x->getidprzeszkody());
                                  pobranieprobki1(dynamic_pointer_cast<Interfejs_SFR>(lazik1),dynamic_pointer_cast<Interfejs_probki>(x));
                                  rysownik->erase_shape(kolekcja_lazikow[idlazika]->getid());
                                  break;
                                  }
                                  else
                                  {
                                  std::cout<<"Nie pobrano próbki"<<std::endl;
                                        
                                  break;
                                  }
                            }
                    }
                break;
                }
        }
         return a;
         
}
 
  

    uint lazikjedz1(int idlazika,double len,shared_ptr<drawNS::Draw3DAPI>rysownik)
    {   
        rysownik->change_ref_time_ms(0);
        rysownik->erase_shape(kolekcja_lazikow[idlazika]->getid());
        double i=0;
        if(len>0){
        while(i<len)
        {   
            if(czykolizja(kolekcja_lazikow.at(idlazika),rysownik,idlazika))
            {
                kolekcja_lazikow[idlazika]->rysuj(rysownik);
                kolekcja_lazikow[idlazika]->przesuniecie(Wektor3D(-0.1,0,0));
                kolekcja_lazikow[idlazika]->aktualizujobiekt();
                rysownik->erase_shape(kolekcja_lazikow[idlazika]->getid());
                rysownik->redraw();
                std::cout<<"KOLIZJA";
                kolekcja_lazikow[idlazika]->rysuj(rysownik);
                wypisanieprobek();
            return kolekcja_lazikow[idlazika]->getid();


            }

                kolekcja_lazikow[idlazika]->rysuj(rysownik);
                kolekcja_lazikow[idlazika]->przesuniecie(Wektor3D(0.1,0,0));
                kolekcja_lazikow[idlazika]->aktualizujobiekt();
                rysownik->erase_shape(kolekcja_lazikow[idlazika]->getid());
                rysownik->redraw();
            i+=0.1;
            
        }
        

        }
        
        else if(len<=0)
        {
            while(i<abs(len))
        {   
            if(czykolizja(kolekcja_lazikow.at(idlazika),rysownik,idlazika))
            {
                    kolekcja_lazikow[idlazika]->rysuj(rysownik);
                kolekcja_lazikow[idlazika]->przesuniecie(Wektor3D(0.1,0,0));
                kolekcja_lazikow[idlazika]->aktualizujobiekt();
                rysownik->erase_shape(kolekcja_lazikow[idlazika]->getid());
                rysownik->redraw();
                std::cout<<"KOLIZJA";
                kolekcja_lazikow[idlazika]->rysuj(rysownik);
            
            return kolekcja_lazikow[idlazika]->getid();
            break;
            }else{
            kolekcja_lazikow[idlazika]->rysuj(rysownik);
            kolekcja_lazikow[idlazika]->przesuniecie(Wektor3D(-0.1,0,0));
            kolekcja_lazikow[idlazika]->aktualizujobiekt();
            rysownik->erase_shape(kolekcja_lazikow[idlazika]->getid());
            rysownik->redraw();
            }
            i+=0.1;
            
            
        }
        }

        kolekcja_lazikow[idlazika]->rysuj(rysownik);
       uint id=kolekcja_lazikow[idlazika]->getid();
        return id;
       
    }

void wypiszprobki(int idlazika){
    
    if(dynamic_pointer_cast<Interfejs_SFR>(kolekcja_lazikow.at(idlazika))!=nullptr)
    {
       dynamic_pointer_cast<Interfejs_SFR>(kolekcja_lazikow.at(idlazika))->wyswietlpobraneprobki();
    }
    else
    {
        std::cout<<"Zły łazik"<<std::endl;
    }
}

/*
    int lazikjedz(int idlazika,double len,shared_ptr<drawNS::Draw3DAPI>rysownik)
    {
        int x=kolekcja_lazikow[idlazika]->animacjajedz(len, rysownik);return x;
    }
*/

    int lazikobrot(int idlazika,int kat,shared_ptr<drawNS::Draw3DAPI>rysownik)
    {
        int x=kolekcja_lazikow[idlazika]->animacjaobrotz(kat, rysownik);return x;
    }


    int pokazlazika(int idlazika, shared_ptr<drawNS::Draw3DAPI>rysownik)
    {
        int x=kolekcja_lazikow[idlazika]->animacjaobrotz(360,rysownik);
        return x;
    }

    void wypisznazwe(int idlazika)
    {
        std::cout<<"Nazwa Łazika     "<<kolekcja_lazikow[idlazika]->getnazwa()<<std::endl;
        std::cout<<"Położenie łazika       "<<kolekcja_lazikow[idlazika]->get_polozenie()<<std::endl;
    }

};





#endif